import numpy as np
import os
import pickle
import tensorflow as tf
import sys
from tensorflow_probability import distributions


class emb_model(object):
    def __init__(self, conf):
        tf.compat.v1.reset_default_graph()
        self.K = conf['emb_dim']
        self.cs = conf['context_window']
        self.ns = conf['negative_sampling']
        self.sig = conf['noise']
        self.dynamic = conf['dynamic']
        self.logdir = conf['logdir']
        self.L = conf['n_words']
        self.T = conf['n_bins']
        self.n_minibatch = conf['minibatch']
        self.unigram = conf['unigram']
        self.n_epochs = conf['n_epochs']
        self.ngram = conf['ngram']
        self.alpha_trainable = conf['alpha_train']
        self.f_batch = conf['f_batch']
        self.n_examples = conf['n_examples']
        if 'init' in conf.keys() and conf['init']:
            self.rho_init = conf['rho']
            self.alpha_init = (np.random.randn(self.L, self.K) / self.K).astype('float32')
        else:
            self.rho_init = (np.random.randn(self.L, self.K) / self.K).astype('float32')
            self.alpha_init = (np.random.randn(self.L, self.K) / self.K).astype('float32')
        if not self.alpha_trainable:
            self.rho_init = (0.1 * np.random.randn(self.L, self.K) / self.K).astype('float32')

    def eval_log_like(self, feed_dict):
        return self.sess.run(tf.log(self.y_pos.mean() + 0.000001), feed_dict=feed_dict)

    def initialize_training(self):
        optimizer = tf.train.AdamOptimizer()
        self.train = optimizer.minimize(self.loss)
        self.sess = tf.Session()
        with self.sess.as_default():
            tf.global_variables_initializer().run()
        with tf.name_scope('objective'):
            tf.summary.scalar('loss', self.loss)
            tf.summary.scalar('priors', self.log_prior)
            tf.summary.scalar('ll_pos', self.ll_pos)
            tf.summary.scalar('ll_neg', self.ll_neg)
        self.summaries = tf.summary.merge_all()
        self.train_writer = tf.summary.FileWriter(self.logdir, self.sess.graph)
        self.saver = tf.train.Saver()

    def train_embeddings(self):
        p = 0
        pj = 0
        bs = self.n_examples
        perc = range(0, 101, 5)
        if self.dynamic:
            self.batch = []
            for t in range(self.T):
                self.batch.append(self.f_batch(t))
        else:
            self.batch = self.f_batch()
        print('Examples: ' + str(bs))
        for step in range(self.n_epochs + 1):
            try:
                while True:
                    feed_dict = {}
                    if self.dynamic:
                        for t in range(self.T):
                            feed_dict[self.placeholders[t]] = self.batch[t].__next__()
                    else:
                        feed_dict[self.placeholder] = self.batch.__next__()
                    if p >= int(bs * perc[pj] / 100):
                        summary, ll_pos, ll_neg, _ = self.sess.run(
                            [self.summaries, self.ll_pos, self.ll_neg, self.train], feed_dict=feed_dict)
                        self.train_writer.add_summary(summary, (self.n_epochs) + step)
                        print('Batches processed of first split : ' + str(perc[pj]) + '%(Examples: ' + str(p) + ')')
                        print("%8d/%8d log-likelihood: %6.4f on positive samples,%6.4f on negative samples " % (
                            step, self.n_epochs, ll_pos, ll_neg))
                        sys.stdout.flush()
                        pj = pj + 1
                    else:
                        self.sess.run([self.train], feed_dict=feed_dict)
                    if self.dynamic:
                        p = p + self.n_minibatch[0]
                    else:
                        p = p + self.n_minibatch.sum()
            except StopIteration:
                if self.dynamic:
                    for t in range(self.T):
                        self.batch[t] = self.f_batch(t)
                else:
                    self.batch = self.f_batch()
            p = 0
            pj = 0
            self.dump(self.logdir + "variational" + ".dat")
        self.saver.save(self.sess, os.path.join(self.logdir, "model.ckpt"), 0)


class bern_emb_model(emb_model):
    def __init__(self, conf):
        super(bern_emb_model, self).__init__(conf)
        self.n_minibatch = self.n_minibatch.sum()

        with tf.name_scope('model'):
            # Data Placeholder
            with tf.name_scope('input'):
                self.placeholder = tf.compat.v1.placeholder(tf.int32, shape=(self.n_minibatch, 2))
                self.words = self.placeholder

            if not self.ngram:
                # Index Masks
                with tf.name_scope('context_mask'):
                    self.p_mask = tf.cast(tf.range(self.cs / 2, self.n_minibatch + int(self.cs / 2)), tf.int32)
                    rows = tf.cast(tf.tile(tf.expand_dims(tf.range(0, self.cs / 2), [0]), [self.n_minibatch, 1]),
                                   tf.int32)
                    columns = tf.cast(
                        tf.tile(tf.expand_dims(tf.range(0, self.n_minibatch), [1]), [1, int(self.cs / 2)]), tf.int32)
                    self.ctx_mask = tf.concat([rows + columns, rows + columns + int(self.cs / 2) + 1], 1)

                with tf.name_scope('embeddings'):
                    self.rho = tf.Variable(self.rho_init, name='rho')
                    self.alpha = tf.Variable(self.alpha_init, name='alpha', trainable=self.alpha_trainable)

                    with tf.name_scope('priors'):
                        prior = distributions.normal(loc=0.0, scale=self.sig)
                        if self.alpha_trainable:
                            self.log_prior = tf.reduce_sum(prior.log_prob(self.rho) + prior.log_prob(self.alpha))
                        else:
                            self.log_prior = tf.reduce_sum(prior.log_prob(self.rho))

                with tf.name_scope('natural_param'):
                    # Taget and Context Indices
                    with tf.name_scope('target_word'):
                        self.p_idx = tf.gather(self.words, self.p_mask)
                        self.p_rho = tf.squeeze(tf.gather(self.rho, self.p_idx))

                    # Negative samples
                    with tf.name_scope('negative_samples'):
                        unigram_logits = tf.tile(tf.expand_dims(tf.log(tf.constant(self.unigram)), [0]),
                                                 [self.n_minibatch, 1])
                        self.n_idx = tf.multinomial(unigram_logits, self.ns)
                        self.n_rho = tf.gather(self.rho, self.n_idx)

                    with tf.name_scope('context'):
                        self.ctx_idx = tf.squeeze(tf.gather(self.words, self.ctx_mask))
                        self.ctx_alphas = tf.gather(self.alpha, self.ctx_idx)

                    # Natural parameter
                    ctx_sum = tf.reduce_sum(self.ctx_alphas, [1])
                    self.p_eta = tf.expand_dims(tf.reduce_sum(tf.multiply(self.p_rho, ctx_sum), -1), 1)
                    self.n_eta = tf.reduce_sum(
                        tf.multiply(self.n_rho, tf.tile(tf.expand_dims(ctx_sum, 1), [1, self.ns, 1])), -1)

            else:
                with tf.name_scope('embeddings'):
                    self.rho = tf.Variable(self.rho_init, name='rho')
                    self.alpha = tf.Variable(self.alpha_init, name='alpha', trainable=self.alpha_trainable)

                    with tf.name_scope('priors'):
                        prior = distributions.normal(loc=0.0, scale=self.sig)
                        if self.alpha_trainable:
                            self.log_prior = tf.reduce_sum(prior.log_prob(self.rho) + prior.log_prob(self.alpha))
                        else:
                            self.log_prior = tf.reduce_sum(prior.log_prob(self.rho))

                with tf.name_scope('natural_param'):
                    # Taget and Context Indices
                    with tf.name_scope('target_word'):
                        hot0 = tf.Variable(initial_value=[1, 0], dtype=tf.int32)
                        self.p_idx = tf.tensordot(self.placeholder, hot0, 1)
                        self.p_rho = tf.squeeze(tf.gather(self.rho, self.p_idx))

                    # Negative samples
                    with tf.name_scope('negative_samples'):
                        unigram_logits = tf.tile(tf.expand_dims(tf.log(tf.constant(self.unigram)), [0]),
                                                 [self.n_minibatch, 1])
                        self.n_idx = tf.multinomial(unigram_logits, self.ns)
                        self.n_rho = tf.gather(self.rho, self.n_idx)

                    with tf.name_scope('context'):
                        hot1 = tf.Variable(initial_value=[0, 1], dtype=tf.int32)
                        ctx_idx_prev = tf.tensordot(self.placeholder, hot1, 1)
                        self.ctx_idx = tf.expand_dims(ctx_idx_prev, 1)
                        self.ctx_alphas = tf.gather(self.alpha, self.ctx_idx)

                    # Natural parameter
                    ctx_sum = tf.reduce_sum(self.ctx_alphas, [1])
                    self.p_eta = tf.expand_dims(tf.reduce_sum(tf.multiply(self.p_rho, ctx_sum), -1), 1)
                    self.n_eta = tf.reduce_sum(
                        tf.multiply(self.n_rho, tf.tile(tf.expand_dims(ctx_sum, 1), [1, self.ns, 1])), -1)

            # Conditional likelihood
            self.y_pos = distributions.bernoulli(logits=self.p_eta)
            self.y_neg = distributions.bernoulli(logits=self.n_eta)

            self.ll_pos = tf.reduce_sum(self.y_pos.log_prob(1.0))
            self.ll_neg = tf.reduce_sum(self.y_neg.log_prob(0.0))

            self.log_likelihood = self.ll_pos + self.ll_neg

            self.loss = - (self.n_epochs * self.log_likelihood + self.log_prior)

    def dump(self, fname):
        with self.sess.as_default():
            dat = {'rho': self.rho.eval(),
                   'alpha': self.alpha.eval()}
        pickle.dump(dat, open(fname, "ab+"))


class dynamic_bern_emb_model(emb_model):
    def __init__(self, conf):
        super(dynamic_bern_emb_model, self).__init__(conf)

        with tf.name_scope('model'):
            with tf.name_scope('embeddings'):
                self.alpha = tf.Variable(self.alpha_init, name='alpha', trainable=self.alpha_trainable)

                self.rho_t = {}
                for t in range(self.T):
                    self.rho_t[t] = tf.Variable(self.rho_init[t]
                                                + 0.001 * tf.random_normal([self.L, self.K]) / self.K,
                                                name='rho_' + str(t))

                with tf.name_scope('priors'):
                    global_prior = distributions.normal(loc=0.0, scale=self.sig)
                    local_prior = distributions.normal(loc=0.0, scale=self.sig / 100.0)

                    self.log_prior = tf.reduce_sum(global_prior.log_prob(self.alpha))
                    self.log_prior += tf.reduce_sum(global_prior.log_prob(self.rho_t[0]))
                    for t in range(1, self.T):
                        self.log_prior += tf.reduce_sum(local_prior.log_prob(self.rho_t[t] - self.rho_t[t - 1]))

            with tf.name_scope('likelihood'):
                self.placeholders = {}
                self.y_pos = {}
                self.y_neg = {}
                self.ll_pos = 0.0
                self.ll_neg = 0.0
                for t in range(self.T):
                    if not self.ngram:
                        # Index Masks
                        p_mask = tf.range(int(self.cs / 2), self.n_minibatch[t] + int(self.cs / 2))
                        rows = tf.tile(tf.expand_dims(tf.range(0, int(self.cs / 2)), [0]), [self.n_minibatch[t], 1])
                        columns = tf.tile(tf.expand_dims(tf.range(0, self.n_minibatch[t]), [1]), [1, int(self.cs / 2)])

                        ctx_mask = tf.concat([rows + columns, rows + columns + int(self.cs / 2) + 1], 1)

                        # Data Placeholder
                        self.placeholders[t] = tf.compat.v1.placeholder(tf.int32, shape=(self.n_minibatch[t] + self.cs))

                        # Taget and Context Indices
                        p_idx = tf.gather(self.placeholders[t], p_mask)
                        ctx_idx = tf.squeeze(tf.gather(self.placeholders[t], ctx_mask))

                        # Negative samples
                        unigram_logits = tf.tile(tf.expand_dims(tf.log(tf.constant(self.unigram)), [0]),
                                                 [self.n_minibatch[t], 1])

                    else:
                        self.placeholders[t] = tf.placeholder(tf.int32, shape=(self.n_minibatch[t], 2))
                        hot0 = tf.Variable(initial_value=[1, 0], dtype=tf.int32)
                        hot1 = tf.Variable(initial_value=[0, 1], dtype=tf.int32)
                        p_idx = tf.tensordot(self.placeholders[t], hot0, 1)
                        ctx_idx_prev = tf.tensordot(self.placeholders[t], hot1, 1)
                        ctx_idx = tf.expand_dims(ctx_idx_prev, 1)

                        # Negative samples
                        unigram_logits = tf.tile(tf.expand_dims(tf.log(tf.constant(self.unigram)), [0]),
                                                 [self.n_minibatch[t], 1])

                    n_idx = tf.multinomial(unigram_logits, self.ns)

                    # Context vectors
                    ctx_alphas = tf.gather(self.alpha, ctx_idx)

                    p_rho = tf.squeeze(tf.gather(self.rho_t[t], p_idx))
                    n_rho = tf.gather(self.rho_t[t], n_idx)

                    # Natural parameter
                    ctx_sum = tf.reduce_sum(ctx_alphas, [1])
                    p_eta = tf.expand_dims(tf.reduce_sum(tf.multiply(p_rho, ctx_sum), -1), 1)
                    n_eta = tf.reduce_sum(tf.multiply(n_rho, tf.tile(tf.expand_dims(ctx_sum, 1), [1, self.ns, 1])), -1)

                    # Conditional likelihood
                    self.y_pos[t] = Bernoulli(logits=p_eta)
                    self.y_neg[t] = Bernoulli(logits=n_eta)

                    self.ll_pos += tf.reduce_sum(self.y_pos[t].log_prob(1.0))
                    self.ll_neg += tf.reduce_sum(self.y_neg[t].log_prob(0.0))

            self.loss = - (self.n_epochs * (self.ll_pos + self.ll_neg) + self.log_prior)

    def dump(self, fname):
        with self.sess.as_default():
            dat = {'alpha': self.alpha.eval()}
            for t in range(self.T):
                dat['rho_' + str(t)] = self.rho_t[t].eval()
        pickle.dump(dat, open(fname, "ab+"))

    def eval_log_like(self, feed_dict):
        log_p = np.zeros((0, 1))
        for t in range(self.T):
            log_p_t = self.sess.run(tf.log(self.y_pos[t].mean() + 0.000001), feed_dict=feed_dict)
            log_p = np.vstack((log_p, log_p_t))
        return log_p
