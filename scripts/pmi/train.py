#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 10 13:10:42 2016

"""

# main script for time CD 
# trainfile has lines of the form
# tok1,tok2,pmi

import numpy as np
import scripts.pmi.util_timeCD as util
import pickle as pickle
import argparse


def print_params(r, lam, tau, gam, emph, ITERS):
    print('rank = {}'.format(r))
    print('frob  regularizer = {}'.format(lam))
    print('time  regularizer = {}'.format(tau))
    print('symmetry regularizer = {}'.format(gam))
    print('emphasize param   = {}'.format(emph))
    print('total iterations = {}'.format(ITERS))


def trainPMI(conf):
    ITERS = conf['iters']
    lam = conf['lambda']
    gam = conf['gamma']
    tau = conf['tau']
    r = conf['rank']
    b = conf['batch']
    nw = b
    emph = conf['emphasize']
    savehead = conf['savepath']
    collection = conf['collection']
    T = conf['T']

    savefile = savehead + 'L' + str(lam) + 'T' + str(tau) + 'G' + str(gam) + 'A' + str(emph)

    print('starting training with following parameters')
    print_params(r, lam, tau, gam, emph, ITERS)
    print('there are a total of {} words, and {} time points'.format(nw, T))

    print('X*X*X*X*X*X*X*X*X')
    print('initializing')

    Ulist, Vlist = util.initvars(nw, T, r)
    # Ulist,Vlist = util.import_static_init(T) modified
    # print(Ulist)
    # print(Vlist)
    print('getting batch indices')
    if b < nw:
        b_ind = util.getbatches(nw, b)
    else:
        b_ind = [[i for i in range(0, nw)]]  # modified

    import time
    start_time = time.time()
    # sequential updates
    for iteration in range(0, ITERS):
        print_params(r, lam, tau, gam, emph, ITERS)
        try:
            Ulist = pickle.load(open("%sngU_iter%d.p" % (savefile, iteration), "rb"))
            Vlist = pickle.load(open("%sngV_iter%d.p" % (savefile, iteration), "rb"))
            print('iteration %d loaded succesfully' % iteration)
            continue
        except(IOError):
            print('ERROR: can\'t load iteration %d' % iteration)  # modified
        loss = 0
        # shuffle times
        if iteration == 0:
            times = T
        else:
            times = np.random.permutation(T)

        for t in range(0, len(times)):  # select a time
            print('iteration %d, time %d' % (iteration, t))

            pmi = collection[t]
            for j in range(0, len(b_ind)):  # select a mini batch
                print('%d out of %d' % (j, len(b_ind)))
                ind = b_ind[j]
                ## UPDATE V
                # get data
                pmi_seg = pmi[:, ind].todense()

                if t == 0:
                    vp = np.zeros((len(ind), r))
                    up = np.zeros((len(ind), r))
                    iflag = True
                else:
                    vp = Vlist[t - 1][ind, :]
                    up = Ulist[t - 1][ind, :]
                    iflag = False

                if t == len(T) - 1:
                    vn = np.zeros((len(ind), r))
                    un = np.zeros((len(ind), r))
                    iflag = True
                else:
                    vn = Vlist[t + 1][ind, :]
                    un = Ulist[t + 1][ind, :]
                    iflag = False
                Vlist[t][ind, :] = util.update(Ulist[t], emph * pmi_seg, vp, vn, lam, tau, gam, ind, iflag)
                Ulist[t][ind, :] = util.update(Vlist[t], emph * pmi_seg, up, un, lam, tau, gam, ind, iflag)

            ####  INNER BATCH LOOP END

        # save
        print('time elapsed = ', time.time() - start_time)

        pickle.dump(Ulist, open("%sngU_iter%d.p" % (savefile, iteration), "wb"), pickle.HIGHEST_PROTOCOL)
        pickle.dump(Vlist, open("%sngV_iter%d.p" % (savefile, iteration), "wb"), pickle.HIGHEST_PROTOCOL)
