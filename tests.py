import unittest
from db_connection import DatabaseOperation


class TestBackEndMethods(unittest.TestCase):
    dbo = DatabaseOperation()

    '''
    def test_isUserAdded(self):
        self.assertEqual(self.dbo.sign_up('admin', 'admin', 'who are you?', 'batman'), True)

    def test_userIsNotAdded(self):
        self.assertEqual(self.dbo.sign_up('admin', 'admin', 'who are you?', 'batman'), False)
    '''

    def test_describeCorpus(self):
        if self.dbo.add_corpus(name='Ngram', path='home/loop/Desktop/Ngram', language='english-partut',
                               is_diachronic=1, p_attributes='year,frquency,book_frequency', username='admin',
                               year_start=1990, year_end=1995, split=10, dim_d=50000, mode='r', visibility='private'):
            self.assertIsNot(self.dbo.view_corpus('NGram', 'private'), '')


if __name__ == '__main__':
    unittest.main()
