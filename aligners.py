import os
import pickle
import numpy as np
from scipy.spatial import procrustes
from scripts.pmi.train import trainPMI
from scripts.bernoulli.models import dynamic_bern_emb_model


class Aligner(object):
    def __init__(self, name):
        self.name = name


class BernoulliAligner(Aligner):

    def __init__(self, name, bin_coll, conf, uni):
        self.name = name
        self.bin_coll = bin_coll.get()
        self.savepath = bin_coll.binpath + 'bernoulli' + os.sep + 'aligned_models' + os.sep
        if not os.path.exists(self.savepath):
            os.makedirs(self.savepath)
            newconf = dict(conf)
            newconf['dynamic'] = True
            newconf['logdir'] = self.savepath
            newconf['n_words'] = len(bin_coll.corpus.D)
            newconf['n_bins'] = len(bin_coll.corpus.years)
            unigram = np.array(uni)
            unigram = (1.0 * unigram / sum(unigram)) ** (3.0 / 4)
            newconf['unigram'] = unigram
            newconf['init'] = True
            newconf['rho'] = []
            for i in range(newconf['n_bins']):
                D = pickle.load(open(bin_coll.binpath + 'bernoulli' + os.sep + 'models' + os.sep + 'model_' + str(
                    i) + os.sep + 'variational0.dat', 'rb'))
                newconf['rho'].append(D['rho'])
            if 'minibatch' in newconf.keys():
                self.mb = newconf['minibatch']
                newconf['minibatch'] = np.array([self.mb] * newconf['n_bins'])
            if type(bin_coll.corpus).__name__ == 'Ngram':
                newconf['ngram'] = True
            else:
                newconf['ngram'] = False
            self.conf = newconf

    def align(self):
        mb = self.mb

        def batch(t):
            examples = self.bin_coll[t].load_examples()
            b = []
            for s in examples:
                if len(b) <= mb - 1:
                    b.append(s)
                else:
                    yield np.array(b, dtype=np.int32)
                    b = []
                    b.append(s)

        self.conf['f_batch'] = batch
        self.conf['n_examples'] = min([b.dim() for b in self.bin_coll])
        model = dynamic_bern_emb_model(self.conf)
        model.initialize_training()
        model.train_embeddings()


class PMI_Aligner(Aligner):
    def __init__(self, name, bincollection, conf):
        self.name = name
        self.conf = dict(conf)
        self.conf['savepath'] = bincollection.binpath + 'pmi' + os.sep + 'aligned_models' + os.sep
        self.conf['collection'] = bincollection.get()
        self.conf['batch'] = len(bincollection.corpus.D) - 1
        self.conf['T'] = bincollection.corpus.years_range

    def align(self):
        trainPMI(self.conf)


class Rotation(Aligner):
    def __init__(self, name, bincollection):
        self.name = name
        self.collection = bincollection.get()
        self.savepath = bincollection.binpath + 'w2v' + os.sep + 'aligned_models' + os.sep

    def align(self):
        if not os.path.exists(self.savepath):
            os.makedirs(self.savepath)

            embs = []

            for model in self.collection:
                embs.append(model)

            for k in range(1, len(embs)):
                embs[k - 1], embs[k], disp = procrustes(embs[k - 1], embs[k])

            embs = np.array(embs)

            with open(self.savepath + 'models.npy', 'wb') as f:
                np.save(f, embs)

            # np.save(self.savepath + 'models.npy', embs)
