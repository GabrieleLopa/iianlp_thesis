import os


class CWBWrapper:

    @staticmethod
    def encode_corpus(corpus_data_dir, document_dir, registry_dir, p_attributes, s_attributes, initial_s_attribute):
        command = 'cwb-encode -d ' + corpus_data_dir + ' -f ' + document_dir + ' -R ' + registry_dir + ' -xsB'

        for p_attribute in p_attributes:
            command += ' -P ' + p_attribute

        for s_attribute in s_attributes:
            command += ' -S ' + s_attribute

        command += ' -0 ' + initial_s_attribute

        os.system(command)

    @staticmethod
    def index_corpus(registry_name, registry_path=None):
        command = 'cwb-make -r ' + registry_path + ' -V ' + registry_name
        os.system(command)

    @staticmethod
    def decode_corpus(registry_name, registry_path, conversion_type, file_name, s_attributes=None, p_attributes=None):
        command = 'cwb-decode -r ' + registry_path + ' ' + conversion_type + ' ' + registry_name

        if s_attributes is None and p_attributes is None:
            command += ' -ALL > ' + file_name
        else:
            for p_attribute in p_attributes:
                command += ' -P ' + p_attribute

            for s_attribute in p_attributes:
                command += ' -S ' + s_attribute

            command += ' > ' + file_name

        os.system(command)

    @staticmethod
    def describe_corpus(registry_name, registry_path):
        command = 'cwb-describe-corpus -r ' + registry_path + ' ' + registry_name

        return os.popen(command).read()

    @staticmethod
    def align_corpus(registry_path, filename, registry_source_name, registry_target_name, alignment_unit):
        command = 'cwb-align -r' + registry_path + ' -o ' + filename + ' ' + registry_source_name + ' ' \
                  + registry_target_name + ' ' + alignment_unit

        os.system(command)

    @staticmethod
    def align_decode(conversion_type, registry_dir, registry_name, p_attributes=None, s_attributes=None):
        command = 'cwb-align-decode'

    @staticmethod
    def align_encode(aligned_file, registry_dir):
        command = 'cwb-align-encode -r ' + registry_dir + ' -V ' + aligned_file

        os.system(command)

    @staticmethod
    def scan_corpus(registry_dir, registry_name, p_attributes, n_results):
        command = 'cwb-scan-corpus -r ' + registry_dir + ' -C ' + registry_name + ' '

        for p_attribute in p_attributes:
            command += p_attribute + ' '

        command += '| sort -nr -k 1 | head -' + str(n_results)

        os.system(command)

