import fileinput
import gzip
import shutil
import tarfile
from math import sqrt
from queue import Queue

from gensim.utils import simple_preprocess
from numpy import unicode, math

from aligners import Rotation
from utilities import Utilities
import os
import xml.etree.cElementTree as ET
from cwb_wrapper import CWBWrapper
import PyCQP_interface
import multiprocessing
from multiprocessing import Pool
from bin import BinCollection
from xml_handler import XMLHandlerThread, XMLHandler
from joblib import Parallel, delayed


class Corpus:

    def __init__(self, name, username=None, visibility=None, p_attributes=None, language=None):
        self.name = name
        self.p_attributes = p_attributes
        self.root = '/home/loop/Desktop' + os.sep + 'CWB'
        if visibility == 'public':
            self.corpus_path = self.root + os.sep + 'Public' + os.sep + self.name
        elif visibility == 'private':
            self.corpus_path = self.root + os.sep + username + os.sep + self.name
        else:
            raise Exception('Invalid corpus visibility!')

        self.documents_path = self.root + os.sep + 'UploadedDocs'
        self.language = language
        self.xml_file_name = self.corpus_path + os.sep + self.name + '.xml'
        self.data_path = self.corpus_path + os.sep + 'data'
        self.registry_path = self.corpus_path + os.sep + 'registry'

        if os.path.exists(self.corpus_path):
            if not os.path.exists(self.data_path):
                os.mkdir(self.data_path)
            if not os.path.exists(self.registry_path):
                os.mkdir(self.registry_path)
        else:
            os.makedirs(self.corpus_path)
            os.mkdir(self.data_path)
            os.mkdir(self.registry_path)

        if not os.path.exists(self.corpus_path + os.path.sep + '%s.xml' % self.name):
            self.create_xml_file()
        if len(os.listdir(self.data_path)) == 0:
            self.encode()
        if len(os.listdir(self.registry_path)) != 0:
            self.index()

    def create_xml_file(self):
        with open(self.xml_file_name, 'w+') as f:
            f.write(
                '<corpus name="%s" corpus_path="%s" language="%s">\n' % (self.name, self.corpus_path, self.language))
        values = os.listdir(self.documents_path)
        for file in values:
            archive = tarfile.open(self.documents_path + os.sep + file, 'r:bz2')
            members = archive.getmembers()
            execution_queue = Queue()
            [execution_queue.put((self, members[i], archive)) for i in range(1, len(members))]
            utils_threads = [XMLHandlerThread(execution_queue) for i in range(1)]
            [thread.start() for thread in utils_threads]
            [thread.join() for thread in utils_threads]
        # with open(self.xml_file_name, 'a+') as f:
        #     f.write('</corpus>')

    def encode(self):
        CWBWrapper.encode_corpus(self.corpus_path + os.sep + 'data', self.xml_file_name,
                                 self.corpus_path + os.sep + 'registry' + os.sep + str(self.name).lower(),
                                 self.p_attributes, ['s:0', 'text:0'], 'corpus:0+name+corpus_path+language')

    def index(self):
        if len(os.listdir(self.corpus_path + os.sep + 'registry')) != 0:
            CWBWrapper.index_corpus(registry_path=self.corpus_path + os.sep + 'registry',
                                    registry_name=str(self.name).upper())
        else:
            raise Exception('You have to encode the corpus first!')

    def delete(self):
        shutil.rmtree(self.corpus_path)

    def add_document(self, document):
        shutil.rmtree(self.data_path)
        shutil.rmtree(self.registry_path)
        os.mkdir(self.data_path)
        os.mkdir(self.registry_path)
        xml = XMLHandler(self)
        doc = None
        with open(self.documents_path + os.sep + document, 'rb') as f:
            doc = xml.model(unicode(f.read(), 'utf-8'))
        xml.write_on_file(doc, document)
        # result = xml.words_per_sentence(document)
        # tree = ET.parse(self.xml_file_name)
        # text = ET.SubElement(tree.getroot(), 'text')
        # text.set('name', document)
        # for key in result.keys():
        #     s = ET.SubElement(text, 's')
        #     s.text = xml.format_sentence(result[key])
        # tree.write(self.xml_file_name)
        self.encode()
        self.index()

    def replace_document(self, old_doc, new_doc):
        self.delete_document(old_doc)
        self.add_document(new_doc)

    def delete_document(self, document):
        can_delete = False
        shutil.rmtree(self.data_path)
        shutil.rmtree(self.registry_path)
        os.mkdir(self.data_path)
        os.mkdir(self.registry_path)
        f = open(self.xml_file_name, 'r')
        lines = f.readlines()
        f.close()
        os.remove(self.xml_file_name)
        w = open(self.xml_file_name, 'w+')
        for line in lines:
            if document in line:
                can_delete = True
            if can_delete == False:
                w.write(line)
        w.close()
        # root = ET.parse(self.xml_file_name).getroot()
        # for child in root:
        #     if child.attrib['name'] == document:
        #         root.remove(child)
        # tree = ET.ElementTree(root)
        # tree.write(self.xml_file_name)
        self.encode()
        self.index()

    def describe(self):
        text = None
        if len(os.listdir(self.corpus_path + os.path.sep + 'registry')) != 0:
            text = CWBWrapper.describe_corpus(registry_name=str(self.name).upper(),
                                              registry_path=self.corpus_path + os.path.sep + 'registry')
        else:
            raise Exception('You have to encode and index the corpus first!')

        return text

    def scan(self, p_attributes, n_results):
        if len(os.listdir(self.corpus_path + os.sep + 'registry')) != 0:
            CWBWrapper.scan_corpus(registry_dir=self.corpus_path + os.sep + 'registry',
                                   registry_name=str(self.name).upper(), p_attributes=p_attributes, n_results=n_results)
        else:
            raise Exception('You have to encode and index the corpus first!')

    def query(self, queries):
        cqp = PyCQP_interface.CQP(bin='cqp', options='-c -r ' + self.corpus_path + os.sep + 'registry')
        cqp.Exec(str(self.name).upper())
        result = ''
        for query in queries:
            result += cqp.Exec(query) + '\n'

        cqp.Terminate()
        return result

    def get_concordances(self, concordances, get_contexts, word, years_range=None):
        concordances = self.query(['A = [word="%s"];' % word, get_contexts])
        return concordances

    def compute_collocations(self, collocations, get_left_context, get_right_context, word, years_range=None):
        # search for the right and left context of a given word
        right_contexts = self.query(['A = "%s";' % word, get_right_context])
        left_contexts = self.query(['A = "%s";' % word, get_left_context])

        # split the results in array of contexts
        right_contexts = right_contexts.split('\n')
        left_contexts = left_contexts.split('\n')
        sums = {}
        occurrences = {}
        means = {}
        offsets = []
        std_devs = {}
        for right_context, left_context in zip(right_contexts, left_contexts):
            # split each context into words
            words_right_context = right_context.split()
            words_left_context = left_context.split()
            # initialize a dictionary with word and the distance of each word from a given word
            [sums.update({r_word: 0, l_word: 0}) for r_word, l_word in
             zip(words_right_context, words_left_context)]
            [occurrences.update({r_word: 0, l_word: 0}) for r_word, l_word in
             zip(words_right_context, words_left_context)]
        for w in sums.keys():
            for right_context, left_context in zip(right_contexts, left_contexts):
                right_context_array = right_context.split()
                left_context_array = left_context.split()
                for i in range(len(right_context_array)):
                    if w == right_context_array[i]:
                        context = i + 1
                        sums[w] += context
                        occurrences[w] += 1
                        offsets.append((right_context, w, context))
                for i in range(len(left_context_array)):
                    if w == left_context_array[i]:
                        context = ((i + 1) - len(left_context))
                        sums[w] += context
                        occurrences[w] += 1
                        offsets.append((left_context, w, context))
        # compute means
        for w in sums.keys():
            mean = sums[w] / occurrences[w]
            means.update({w: mean})
        # compute standard deviation
        for w in means:
            variance = 0
            for context, wor, offset in offsets:
                if w == wor and (occurrences[w] - 1 != 0):
                    variance += pow((offset - means[w]), 2)
            if (occurrences[w] - 1) != 0:
                std_dev = sqrt(variance / (occurrences[w] - 1))
                std_devs.update({w: std_dev})
        collocations = sorted(std_devs.items(), key=lambda x: x[1], reverse=False)
        return collocations

    def words_per_sentence(self, input_path):
        dict = {}
        text = ''
        input = open(self.corpus.documents_path + os.path.sep + input_path, 'r+')

        for line in input.readlines():
            text += line

        input.close()

        doc = self.model(text)

        for sentence in doc.sents:
            words = [(token.text, token.pos_, token.lemma_) for token in sentence]
            dict.update({str(sentence): words})

        self.dict = dict

        return dict

class DiachronicCorpus(Corpus):

    def __init__(self, name, username=None, visibility=None, p_attributes=None, language=None):
        super().__init__(name, username, visibility, p_attributes, language)
        self.time_slices_path = self.corpus_path + os.sep + 'time_slices'

        if not os.path.exists(self.time_slices_path):
            os.mkdir(self.time_slices_path)

    # def create_xml_tree(self):
    # if self.name == 'Ngram':
    #     p = Pool(multiprocessing.cpu_count())
    #     values = os.listdir(self.documents_path)
    #     results = p.map(func=self.read_files, iterable=values)
    #     for testo, doc in results:
    #         document = ET.SubElement(root, 'document')
    #         document.set('name', doc)
    #         document.text = testo
    #
    #     p.close()
    # else:

    # logDice
    def collocation_sketch_engine(self, get_left_context, get_right_context, word, years_range=None):
        if years_range is None:
            raise Exception('years_range cannot be None')
        dictionary = {}
        cooccurences = {}
        right_contexts = ''
        left_contexts = ''
        years = self.query(['A = "%s";' % word, 'tabulate A match year;'])
        for year in years_range:
            if str(year) in years.split():
                right_contexts += self.query(['A = "%s" [year="%s"];' % (word, year), get_right_context])
                left_contexts += self.query(['A = "%s" [year="%s"];' % (word, year), get_left_context])
        right_contexts = right_contexts.split('\n')
        left_contexts = left_contexts.split('\n')
        word_occurrences = len(right_contexts)

        for right_context, left_context in zip(right_contexts, left_contexts):
            words_right_context = right_context.split()
            words_left_context = left_context.split()
            [dictionary.update({r_word: 0, l_word: 0}) for r_word, l_word in zip(words_right_context, words_left_context)]
            [cooccurences.update({'%s-%s' % (word, r_word): 0, '%s-%s' % (word, l_word): 0}) for r_word, l_word in zip(words_right_context, words_left_context)]

        for w in dictionary.keys():
            for right_context, left_context in zip(right_contexts, left_contexts):
                if w in right_context or w in left_context:
                    dictionary[w] += 1

        for right_context, left_context in zip(right_contexts, left_contexts):
            for cooccurence in cooccurences.keys():
                w = cooccurence.split('-')[1]
                if w in right_context or w in left_context:
                    cooccurences[cooccurence] += 1

        log_dices = {}

        for w in dictionary.keys():
            dice = (2 * cooccurences['%s-%s' % (word, w)]) / (word_occurrences + dictionary[w])
            log_dice = 14 + math.log2(dice)
            log_dices.update({w: log_dice})

        return log_dices

    def compute_collocations(self, collocations, get_left_context, get_right_context, word, years_range=None):
        if years_range is None:
            raise Exception('years_range cannot be None')
        # search for the right and left context of a given word
        right_contexts = ''
        left_contexts = ''
        years = self.query(['A = "%s";' % word, 'tabulate A match year;'])
        for year in years_range:
            if str(year) in years.split():
                right_contexts += self.query(['A = "%s" [year="%s"];' % (word, year), get_right_context])
                left_contexts += self.query(['A = "%s" [year="%s"];' % (word, year), get_left_context])

        # split the results in array of contexts
        right_contexts = right_contexts.split('\n')
        left_contexts = left_contexts.split('\n')
        sums = {}
        occurrences = {}
        means = {}
        offsets = []
        std_devs = {}
        for right_context, left_context in zip(right_contexts, left_contexts):
            # split each context into words
            words_right_context = right_context.split()
            words_left_context = left_context.split()
            # initialize a dictionary with word and the distance of each word from a given word
            [sums.update({r_word: 0, l_word: 0}) for r_word, l_word in
             zip(words_right_context, words_left_context)]
            [occurrences.update({r_word: 0, l_word: 0}) for r_word, l_word in
             zip(words_right_context, words_left_context)]
        for w in sums.keys():
            for right_context, left_context in zip(right_contexts, left_contexts):
                right_context_array = right_context.split()
                left_context_array = left_context.split()
                for i in range(len(right_context_array)):
                    if w == right_context_array[i]:
                        context = i + 1
                        sums[w] += context
                        occurrences[w] += 1
                        offsets.append((right_context, w, context))
                for i in range(len(left_context_array)):
                    if w == left_context_array[i]:
                        context = ((i + 1) - len(left_context))
                        sums[w] += context
                        occurrences[w] += 1
                        offsets.append((left_context, w, context))
        # compute means
        for w in sums.keys():
            mean = sums[w] / occurrences[w]
            means.update({w: mean})
        # compute standard deviation
        for w in means:
            variance = 0
            for context, wor, offset in offsets:
                if w == wor and (occurrences[w] - 1 != 0):
                    variance += pow((offset - means[w]), 2)
            if (occurrences[w] - 1) != 0:
                std_dev = sqrt(variance / (occurrences[w] - 1))
                std_devs.update({w: std_dev})
        collocations = sorted(std_devs.items(), key=lambda x: x[1], reverse=False)
        return collocations

    def get_concordances(self, concordances, get_contexts, word, years_range=None):
        if years_range is None:
            raise Exception('years_range is None')
        years = self.query(['A = [word="%s"];' % word, 'tabulate A match year;'])
        years = years.split()
        for year in years_range:
            if str(year) in years:
                concordances += self.query(['A = [word="%s"] [year="%s"];' % (word, year), get_contexts])
        return concordances

    def timeseries(self, split, timeseries_cumulative, timeseries_pointwise, ut, years_range):
        if len(os.listdir(self.time_slices_path)) == 0:
            corpus_years = self.generate_time_slices(years_range)
            self.generate_dictionary()
            dictionary = ut.load_dictionary(self.corpus_path)
            keys = list(dictionary.keys())[0:50000]
            dictionary = {k: dictionary[k] for k in keys}
            coll = BinCollection(corpus=self, conf=ut.get_conf('WORD2VEC'), years_range=corpus_years,
                                 split=split, dictionary=dictionary, mode='WORD2VEC')
            rotation = Rotation(name=self.name, bincollection=coll)
            rotation.align()
            timeseries_cumulative, timeseries_pointwise = ut.create_timeseries(self, dictionary, corpus_years,
                                                                               split)
        else:
            [os.remove(self.time_slices_path + os.sep + element) for element in os.listdir(self.time_slices_path)]
            shutil.rmtree(self.corpus_path + os.sep + 'bin')
            shutil.rmtree(self.corpus_path + os.sep + 'timeseries')
            os.remove(self.corpus_path + os.sep + 'dictionary.txt')
            corpus_years = self.generate_time_slices(years_range)
            self.generate_dictionary()
            dictionary = ut.load_dictionary(self.corpus_path)
            keys = list(dictionary.keys())[0:50000]
            dictionary = {k: dictionary[k] for k in keys}
            coll = BinCollection(corpus=self, conf=ut.get_conf('WORD2VEC'), years_range=corpus_years,
                                 split=split,
                                 dictionary=dictionary, mode='WORD2VEC')
            rotation = Rotation(name=self.name, bincollection=coll)
            rotation.align()
            timeseries_cumulative, timeseries_pointwise = ut.create_timeseries(self, dictionary, corpus_years,
                                                                               split)
        return timeseries_cumulative, timeseries_pointwise

    def generate_time_slices(self, years_range):
        # corpus_years = self.query(['A = [];', 'tabulate A match year;'])
        # corpus_years_set = {int(year) for year in corpus_years.split('\n') if year.isnumeric() and (int(year) in years_range)}
        if len(os.listdir(self.time_slices_path)) == 0:
            for year in years_range:
                # if year in corpus_years_set:
                self.query(
                        ['A = [year="%s"] expand to s;' % year,
                        'tabulate A match .. matchend lemma > "{}";'.format((self.time_slices_path + os.sep + str(year) + '.txt'))])

        return years_range # list(corpus_years_set)

    def generate_dictionary(self):
        years = os.listdir(self.time_slices_path)
        pool = Pool(multiprocessing.cpu_count())
        dictionaries = pool.map(func=self.generate_time_slice_dictionary, iterable=years)
        pool.close()
        first = dictionaries[0]
        for i in range(1, len(dictionaries)):
            first = first.intersection(dictionaries[i])
        w = open(self.corpus_path + os.sep + 'dictionary.txt', 'w')
        [w.write(word + '\n') for word in first]
        w.close()

    def generate_time_slice_dictionary(self, year):
        r = open(self.time_slices_path + os.sep + year, 'r')
        dictionary = set()
        for line in r.readlines():
            words = line.split()
            [dictionary.add(word) for word in words]
        r.close()
        return dictionary

    '''
    tuning del vettore da passare a gensim secondo il paper: Fast Training of word2vec Representations Using N-gram Corpora
    '''

    # def get_sentences(self, split, years):
    #     class MySentences(object):
    #         def __init__(self, ylist):
    #             self.ylist = ylist
    #
    #         def __iter__(self):
    #             for y in self.ylist:
    #                 with open(y, 'r') as f:
    #                     for line in f:
    #                         words = line.split()
    #                         genvec = []
    #                         for w in range(1, len(words) - 1):
    #                             genvec.append([words[0], words[w]])
    #                             genvec.append([words[len(words) - 1], words[w]])
    #                             genvec.append([words[w], words[0]])
    #                             genvec.append([words[w], words[len(words) - 1]])
    #                         for c in genvec:
    #                             yield c
    #
    #     return MySentences(years[split])

    def get_sentences(self, year):
        class GutenbergSentences(object):
            def __init__(self, directory):
                self.dir = directory

            def __iter__(self):
                with open(self.dir, 'r') as file:
                    for line in file:
                        yield line.split()

        return GutenbergSentences(year)

    def format_sentences(self, year):
        # gensim_vector = []
        with open(year, 'r') as file:
            for line in file:
                words = line.split()
                genvec = []
                for w in range(1, len(words) - 1):
                    genvec.append([words[0], words[w]])
                    genvec.append([words[len(words) - 1], words[w]])
                    genvec.append([words[w], words[0]])
                    genvec.append([words[w], words[len(words) - 1]])
                for c in genvec:
                    yield c
                # words = line.split()
                # first_gram = words[0]
                # last_gram = words[len(words) - 1]
                # if len(words) == 1:
                #     gensim_vector.append([first_gram])
                # elif len(words) == 2:
                #     gensim_vector.append([first_gram, last_gram])
                #     gensim_vector.append([last_gram, first_gram])
                # elif len(words) > 2:
                #     gensim_vector.append([first_gram, last_gram])
                #     gensim_vector.append([last_gram, first_gram])
                #     for word in range(1, len(words) - 1):
                #         gensim_vector.append([first_gram, word])
                #         gensim_vector.append([last_gram, word])
        return genvec


if __name__ == '__main__':
    c = Corpus(name='Ngram', corpus_path='/home/loop/Desktop/Ngram', documents_path='/home/loop/Desktop/doc',
               language='english-partut', is_diachronic=True, p_attributes=['year', 'frequency', 'book_frequency'],
               years_range=range(1900, 2000))
    u = Utilities(corpus=c, conf_file='conf.txt')
    coll = BinCollection(c, u.get_conf('WORD2VEC'), mode='WORD2VEC')
