import csv
from random import shuffle

import spacy_udpipe
import os
import numpy as np
from threading import Thread
from sklearn.metrics.pairwise import cosine_distances
from scipy.stats import cosine
from scipy import spatial

# class UtilitiesThread(Thread):
#     def __init__(self, corpus, document):
#         Thread.__init__(self)
#         self.result = None
#         self.corpus = corpus
#         self.document = document
#
#     def run(self):
#         utils = Utilities(self.corpus)
#         self.result = utils.words_per_sentence(self.document)


class Utilities:
    models_dic = {'estonian-ewt': 'estonian-ewt-ud-2.5-191206.udpipe',
                  'turkish-imst': 'turkish-imst-ud-2.5-191206.udpipe',
                  'finnish-ftb': 'finnish-ftb-ud-2.5-191206.udpipe',
                  'lithuanian-alksnis': 'lithuanian-alksnis-ud-2.5-191206.udpipe',
                  'english-partut': 'english-partut-ud-2.5-191206.udpipe',
                  'belarusian-hse': 'belarusian-hse-ud-2.5-191206.udpipe',
                  'hungarian-szeged': 'hungarian-szeged-ud-2.5-191206.udpipe',
                  'chinese-gsdsimp': 'chinese-gsdsimp-ud-2.5-191206.udpipe',
                  'urdu-udtb': 'urdu-udtb-ud-2.5-191206.udpipe',
                  'italian-twittiro': 'italian-twittiro-ud-2.5-191206.udpipe',
                  'estonian-edt': 'estonian-edt-ud-2.5-191206.udpipe',
                  'maltese-mudt': 'maltese-mudt-ud-2.5-191206.udpipe',
                  'coptic-scriptorium': 'coptic-scriptorium-ud-2.5-191206.udpipe',
                  'german-gsd': 'german-gsd-ud-2.5-191206.udpipe',
                  'chinese-gsd': 'chinese-gsd-ud-2.5-191206.udpipe',
                  'norwegian-bokmaal': 'norwegian-bokmaal-ud-2.5-191206.udpipe',
                  'lithuanian-hse': 'lithuanian-hse-ud-2.5-191206.udpipe',
                  'hebrew-htb': 'hebrew-htb-ud-2.5-191206.udpipe',
                  'serbian-set': 'serbian-set-ud-2.5-191206.udpipe',
                  'portuguese-bosque': 'portuguese-bosque-ud-2.5-191206.udpipe',
                  'korean-gsd': 'korean-gsd-ud-2.5-191206.udpipe',
                  'old_french-srcmf': 'old_french-srcmf-ud-2.5-191206.udpipe',
                  'romanian-rrt': 'romanian-rrt-ud-2.5-191206.udpipe',
                  'czech-cltt': 'czech-cltt-ud-2.5-191206.udpipe',
                  'norwegian-nynorsk': 'norwegian-nynorsk-ud-2.5-191206.udpipe',
                  'slovak-snk': 'slovak-snk-ud-2.5-191206.udpipe',
                  'finnish-tdt': 'finnish-tdt-ud-2.5-191206.udpipe',
                  'english-gum': 'english-gum-ud-2.5-191206.udpipe',
                  'hindi-hdtb': 'hindi-hdtb-ud-2.5-191206.udpipe',
                  'armenian-armtdp': 'armenian-armtdp-ud-2.5-191206.udpipe',
                  'norwegian-nynorsklia': 'norwegian-nynorsklia-ud-2.5-191206.udpipe',
                  'latvian-lvtb': 'latvian-lvtb-ud-2.5-191206.udpipe',
                  'bulgarian-btb': 'bulgarian-btb-ud-2.5-191206.udpipe',
                  'english-lines': 'english-lines-ud-2.5-191206.udpipe',
                  'english-ewt': 'english-ewt-ud-2.5-191206.udpipe',
                  'galician-ctg': 'galician-ctg-ud-2.5-191206.udpipe',
                  'dutch-alpino': 'dutch-alpino-ud-2.5-191206.udpipe',
                  'old_russian-torot': 'old_russian-torot-ud-2.5-191206.udpipe',
                  'vietnamese-vtb': 'vietnamese-vtb-ud-2.5-191206.udpipe',
                  'french-partut': 'french-partut-ud-2.5-191206.udpipe',
                  'classical_chinese-kyoto': 'classical_chinese-kyoto-ud-2.5-191206.udpipe',
                  'portuguese-gsd': 'portuguese-gsd-ud-2.5-191206.udpipe',
                  'korean-kaist': 'korean-kaist-ud-2.5-191206.udpipe',
                  'galician-treegal': 'galician-treegal-ud-2.5-191206.udpipe',
                  'slovenian-sst': 'slovenian-sst-ud-2.5-191206.udpipe',
                  'czech-fictree': 'czech-fictree-ud-2.5-191206.udpipe',
                  'russian-taiga': 'russian-taiga-ud-2.5-191206.udpipe',
                  'tamil-ttb': 'tamil-ttb-ud-2.5-191206.udpipe',
                  'japanese-gsd': 'japanese-gsd-ud-2.5-191206.udpipe',
                  'persian-seraji': 'persian-seraji-ud-2.5-191206.udpipe',
                  'ukrainian-iu': 'ukrainian-iu-ud-2.5-191206.udpipe',
                  'french-gsd': 'french-gsd-ud-2.5-191206.udpipe',
                  'latin-perseus': 'latin-perseus-ud-2.5-191206.udpipe',
                  'dutch-lassysmall': 'dutch-lassysmall-ud-2.5-191206.udpipe',
                  'croatian-set': 'croatian-set-ud-2.5-191206.udpipe',
                  'italian-partut': 'italian-partut-ud-2.5-191206.udpipe',
                  'irish-idt': 'irish-idt-ud-2.5-191206.udpipe',
                  'spanish-ancora': 'spanish-ancora-ud-2.5-191206.udpipe',
                  'czech-cac': 'czech-cac-ud-2.5-191206.udpipe',
                  'uyghur-udt': 'uyghur-udt-ud-2.5-191206.udpipe',
                  'gothic-proiel': 'gothic-proiel-ud-2.5-191206.udpipe',
                  'spanish-gsd': 'spanish-gsd-ud-2.5-191206.udpipe',
                  'old_church_slavonic-proiel': 'old_church_slavonic-proiel-ud-2.5-191206.udpipe',
                  'polish-pdb': 'polish-pdb-ud-2.5-191206.udpipe',
                  'french-sequoia': 'french-sequoia-ud-2.5-191206.udpipe',
                  'romanian-nonstandard': 'romanian-nonstandard-ud-2.5-191206.udpipe',
                  'italian-vit': 'italian-vit-ud-2.5-191206.udpipe',
                  'north_sami-giella': 'north_sami-giella-ud-2.5-191206.udpipe',
                  'marathi-ufal': 'marathi-ufal-ud-2.5-191206.udpipe',
                  'greek-gdt': 'greek-gdt-ud-2.5-191206.udpipe',
                  'wolof-wtb': 'wolof-wtb-ud-2.5-191206.udpipe',
                  'latin-ittb': 'latin-ittb-ud-2.5-191206.udpipe',
                  'scottish_gaelic-arcosg': 'scottish_gaelic-arcosg-ud-2.5-191206.udpipe',
                  'italian-postwita': 'italian-postwita-ud-2.5-191206.udpipe',
                  'russian-gsd': 'russian-gsd-ud-2.5-191206.udpipe',
                  'arabic-padt': 'arabic-padt-ud-2.        # Thread.__init__(self)5-191206.udpipe',
                  'german-hdt': 'german-hdt-ud-2.5-191206.udpipe',
                  'ancient_greek-perseus': 'ancient_greek-perseus-ud-2.5-191206.udpipe',
                  'italian-isdt': 'italian-isdt-ud-2.5-191206.udpipe',
                  'slovenian-ssj': 'slovenian-ssj-ud-2.5-191206.udpipe',
                  'telugu-mtg': 'telugu-mtg-ud-2.5-191206.udpipe',
                  'polish-lfg': 'polish-lfg-ud-2.5-191206.udpipe',
                  'ancient_greek-proiel': 'ancient_greek-proiel-ud-2.5-191206.udpipe',
                  'swedish-lines': 'swedish-lines-ud-2.5-191206.udpipe',
                  'czech-pdt': 'czech-pdt-ud-2.5-191206.udpipe',
                  'catalan-ancora': 'catalan-ancora-ud-2.5-191206.udpipe',
                  'latin-proiel': 'latin-proiel-ud-2.5-191206.udpipe',
                  'swedish-talbanken': 'swedish-talbanken-ud-2.5-191206.udpipe',
                  'afrikaans-afribooms': 'afrikaans-afribooms-ud-2.5-191206.udpipe',
                  'indonesian-gsd': 'indonesian-gsd-ud-2.5-191206.udpipe',
                  'basque-bdt': 'basque-bdt-ud-2.5-191206.udpipe',
                  'russian-syntagrus': 'russian-syntagrus-ud-2.5-191206.udpipe',
                  'french-spoken': 'french-spoken-ud-2.5-191206.udpipe',
                  'danish-ddt': 'danish-ddt-ud-2.5-191206.udpipe'}

    pmi = {'front_window': int, 'back_window': int, 'iters': int, 'lambda': float, 'gamma': float, 'tau': float,
           'rank': int, 'emphasize': bool}
    w2v = {'min_count': int, 'window': int, 'size': int, 'sample': float, 'negative': int, 'epochs': int,
           'alpha': float}
    ber = {'emb_dim': int, 'context_window': int, 'negative_sampling': int, 'noise': float, 'minibatch': int,
           'n_epochs': int, 'alpha_train': bool}
    dynamic_ber = {'emb_dim': int, 'context_window': int, 'negative_sampling': int, 'noise': float, 'minibatch': int,
                   'n_epochs': int, 'alpha_train': bool}
    corpus = {}
    schema = {'BERNOULLI': ber, 'CORPUS': corpus, 'PMI': pmi, 'DYNAMIC-BERNOULLI': dynamic_ber, 'WORD2VEC': w2v}

    def __init__(self, corpus, conf_file=None):
        self.corpus = corpus
        self.conf_file = conf_file
        self.modules = {}
        self.model = spacy_udpipe.load_from_path(lang=self.corpus.language,
                                                 path='udpipe_models' + os.path.sep + Utilities.models_dic[self.corpus.language])
        self.model.max_length = 1822000
        if self.conf_file is not None:
            self.read_conf()

    def read_conf(self):
        m = ''
        with open(self.conf_file, 'r') as f:
            for line in f:
                if len(line) > 1:
                    line = line[0:len(line) - 1]
                    if line[0] == '#':
                        m = line[1:]
                        self.modules[m] = {}
                    else:
                        line = line.split()
                        self.modules[m][line[0]] = self.schema[m][line[0]](line[1])

    def get_conf(self, module):
        return self.modules[module]

    def timeserie(self, path, mode, approach):

        def load_matrix(m):

            def average_vector(i, j):
                av = m[i][j]
                n = 1
                for k in range(i - 1, -1, -1):
                    av = av + m[k][j]
                    n = n + 1
                return av / n

            TS = np.zeros((m.shape[1], m.shape[0] - 1))
            for i in range(1, m.shape[0]):
                for j in range(m.shape[1]):
                    if approach == 'cumulative':
                        previous = average_vector(i - 1, j)
                    else:
                        previous = m[i - 1][j]
                    TS[j][i - 1] = 1 - spatial.distance.cosine(previous, m[i][j])
            return TS

        modelspath = '%sbin%sw2v%saligned_models%smodels.npy' % (os.sep, os.sep, os.sep, os.sep)
        t = np.load(path + modelspath.format(mode))

        return load_matrix(t)

    def create_timeseries(self, corpus, dictionary, years_range, split):
        if not os.path.exists(corpus.corpus_path + os.sep + 'timeseries' + os.sep):
            os.mkdir(corpus.corpus_path + os.sep + 'timeseries' + os.sep)
            header = ["id", "word"] + [i for i in years_range if i % 10 == 0][:split + 1]
            D = dictionary
            timeseries_cumulative = None
            timeseries_pointwise = None
            for folder in os.listdir(corpus.corpus_path + os.sep + 'bin' + os.sep):
                if folder == 'pmi':
                    T = self.timeserie(corpus.corpus_path, folder, 'point-wise')
                    with open(corpus.corpus_path + os.sep + 'timeseries' + os.sep + folder + '_pw.csv', mode='w+') as f:
                        writer = csv.writer(f, delimiter=';')
                        writer.writerow(header)
                        for r in range(T.shape[0]):
                            writer.writerow([r, D[r]] + list(T[r]))
                    T = self.timeserie(corpus.corpus_path, folder, 'cumulative')
                    with open(corpus.corpus_path + os.sep + 'timeseries' + os.sep + folder + '_cu.csv', mode='w+') as f:
                        writer = csv.writer(f, delimiter=';')
                        writer.writerow(header)
                        for r in range(T.shape[0]):
                            writer.writerow([r, D[r]] + list(T[r]))
                if folder == 'w2v':
                    timeseries_pointwise = self.timeserie(corpus.corpus_path, folder, 'point-wise')
                    with open(corpus.corpus_path + os.sep + 'timeseries' + os.sep + folder + '_pw.csv', mode='w+') as f:
                        writer = csv.writer(f, delimiter=';')
                        writer.writerow(header)
                        for r in range(timeseries_pointwise.shape[0]):
                            writer.writerow([r, D[r]] + list(timeseries_pointwise[r]))
                    timeseries_cumulative = self.timeserie(corpus.corpus_path, folder, 'cumulative')
                    with open(corpus.corpus_path + os.sep + 'timeseries' + os.sep + folder + '_cu.csv', mode='w+') as f:
                        writer = csv.writer(f, delimiter=';')
                        writer.writerow(header)
                        for r in range(timeseries_cumulative.shape[0]):
                            writer.writerow([r, D[r]] + list(timeseries_cumulative[r]))
            return timeseries_cumulative, timeseries_pointwise

    def change_points_experimentation(self, timeseries):
        change_points = []
        mean = np.mean(timeseries)
        i = 0
        for timeserie in timeseries:
            if timeserie < mean:
                change_points.append(i)
            i += 1
        return change_points

    def computeCPD_point(self, timeserie, confidance, samples, points, offset):
        def cumsum(t):
            cs = []
            cs.append(0)
            mean = np.mean(t)
            for i in range(0, len(t)):
                cs.append(cs[i] + t[i] - mean)
            return cs

        cs = cumsum(timeserie)
        sdiff = max(cs) - min(cs)
        c = 0

        for i in range(samples):
            t = timeserie.copy()
            shuffle(t)
            ctemp = cumsum(t)
            difftemp = max(ctemp) - min(ctemp)
            if difftemp < sdiff:
                c = c + 1

        cp = -1
        m = float('-inf')
        for i in range(len(cs)):
            if abs(cs[i]) > m:
                m = abs(cs[i])
                cp = i

        j = cp - 1 + offset

        if c / samples >= confidance:
            points.append((c / samples, m, j))
            self.computeCPD_point(timeserie[:j], confidance, samples, points, 0)
            self.computeCPD_point(timeserie[j + 1:], confidance, samples, points, j + 1)

    def computeCPD(self, timeseries, confidence=0.95, samples=1000, offset=0, filter_zero=False):
        wordscp = {}
        for idx, t in enumerate(timeseries):
            points = []
            self.computeCPD_point(t, confidance=confidence, samples=samples, points=points, offset=offset)
            if filter_zero:
                for i in range(len(points), 0, -1):
                    if t[points[i][2]] <= 0:
                        del t[points[i][2]]
            if len(points) > 0:
                for p in points:
                    wordscp[idx] = (p[0], p[2])
        return wordscp

    def load_years(self, years_range, corpus_path, split):
        years = []
        years.append([])
        j = 1
        s = 0
        for i in years_range:
            pathyear = corpus_path + os.sep + 'time_slices' + os.sep + str(i) + '.txt'
            if os.stat(pathyear).st_size > 0:
                years[s].append(pathyear)
                j = j + 1
            if j >= split + 1 and i <= years_range[len(years_range) - 1]:
                if len(years[s]) == 0:
                    years.pop()
                j = 1
                years.append([])
                s = s + 1
        if len(years[s]) == 0:
            years.pop()
        return years

    def load_dictionary(self, corpus_path):
        D = {}
        # unigram = []
        with open(corpus_path + os.sep + 'dictionary.txt') as f:
            for line in f:
                w = line.split()[0]
                D[len(D)] = w
                # unigram.append(float(line.split()[1]))

        return D # , unigram

    # def run(self):
    #     self.words_per_sentence()
    #
    def words_per_sentence(self, input_path):
        dict = {}
        text = ''
        input = open(self.corpus.documents_path + os.path.sep + input_path, 'r+')

        for line in input.readlines():
            text += line

        input.close()

        doc = self.model(text)

        for sentence in doc.sents:
            words = [(token.text, token.pos_, token.lemma_) for token in sentence]
            dict.update({str(sentence): words})

        self.dict = dict

        return dict

    def format_sentence(self, dictionary):
        string = ''

        for token in dictionary:
            string += token[0] + '\t' + token[1] + '\t' + token[2] + '\n'

        return string
