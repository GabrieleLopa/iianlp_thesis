import os
import sys
from math import sqrt

import mysql.connector

from aligners import BernoulliAligner, Rotation
from bin import BinCollection
from corpus import Corpus, DiachronicCorpus
import shutil

from utilities import Utilities


class User:

    def __init__(self, username, password):
        self.username = username
        self.password = password


class DatabaseOperation:

    def __init__(self):
        self.cnx = mysql.connector.connect(database='BackEndDB', password='admin', host='localhost', user='admin')

    def sign_up(self, name, surname, date_of_birth, username, password, question=None, answer=None):
        is_added = False
        try:
            cursor = self.cnx.cursor()
            add_user = 'INSERT INTO user VALUES ("%s", "%s", \'%s\', "%s", "%s", "%s", "%s");' % (
                name, surname, date_of_birth, username, password, question, answer)
            cursor.execute(add_user)
            self.cnx.commit()
            cursor.close()
            is_added = not is_added
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return is_added

    def sign_in(self, username):
        exists = False
        try:
            cursor = self.cnx.cursor()
            check_user = 'SELECT EXISTS(SELECT * FROM user WHERE username="%s");' % (username)
            cursor.execute(check_user)
            result = cursor.fetchone()
            exists = result[0] == 1
            self.cnx.commit()
            cursor.close()
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return exists

    def unshare_corpus(self, owner, corpus, guest):
        check_sharing_existance = 'SELECT EXISTS(SELECT guest FROM granting_access WHERE owner="%s" AND corpus="%s" AND guest="%s");' % (
            owner, corpus, guest)
        cursor = self.cnx.cursor(buffered=True)
        cursor.execute(check_sharing_existance)
        is_shared = cursor.fetchone()[0]
        self.cnx.commit()
        if is_shared == 1:
            unshare_command = 'DELETE FROM granting_access WHERE owner="%s" AND corpus="%s" AND guest="%s";' % (
                owner, corpus, guest)
            cursor.execute(unshare_command)
            self.cnx.commit()
        else:
            raise Exception('The corpus is not shared')
        cursor.close()

    def modify_permission(self, owner, corpus, guest, new_permission):
        check_sharing_existance = 'SELECT EXISTS(SELECT guest FROM granting_access WHERE owner="%s" AND corpus="%s" AND guest="%s");' % (
            owner, corpus, guest)
        cursor = self.cnx.cursor(buffered=True)
        cursor.execute(check_sharing_existance)
        is_shared = cursor.fetchone()[0]
        self.cnx.commit()
        if is_shared == 1:
            modify_permission_command = 'UPDATE granting_access SET mode=\'%s\' WHERE owner="%s" AND corpus="%s" AND guest="%s";' % (
                new_permission, owner, corpus, guest)
            cursor.execute(modify_permission_command)
            self.cnx.commit()
        else:
            raise Exception('The corpus is not shared')
        cursor.close()

    def delete_account(self, username, password):
        is_deleted = False
        try:
            cursor = self.cnx.cursor()
            check_user = 'SELECT EXISTS(SELECT * FROM user WHERE username="%s" AND password="%s");' % (
                username, password)
            cursor.execute(check_user)
            result = cursor.fetchone()[0]
            if result == 1:
                delete_user = 'DELETE FROM user WHERE username="%s";' % username
                cursor.execute(delete_user)
                is_deleted = not is_deleted
            self.cnx.commit()
            cursor.close()
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return is_deleted

    def password_recovery(self, username, answer):
        password = 'No password!'
        try:
            cursor = self.cnx.cursor()
            check_answer = 'SELECT password FROM user WHERE username="%s" AND answer="%s";' % (username, answer)
            cursor.execute(check_answer)
            result = cursor.fetchone()
            if result is not None:
                password = result[0]
            self.cnx.commit()
            cursor.close()
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return password

    def add_corpus(self, name, language, is_diachronic, p_attributes, username, visibility):
        is_added = False
        try:
            cursor = self.cnx.cursor()
            if visibility == 'private':
                add_corpus = 'INSERT INTO private_corpus VALUES("%s", "%s", "%s", "%r", "%s");' % (
                    name, username, language, self.bool_converter(is_diachronic), p_attributes)
                cursor.execute(add_corpus)
                self.cnx.commit()
                cursor.close()

                p_attr = p_attributes.split(',')

                try:
                    if is_diachronic:
                        DiachronicCorpus(name=name, username=username, visibility=visibility, language=language,
                                         p_attributes=p_attr)
                    else:
                        Corpus(name=name, username=username, visibility=visibility, language=language,
                               p_attributes=p_attr)
                    is_added = not is_added
                except:
                    cursor = self.cnx.cursor()
                    rollback = 'DELETE FROM private_corpus WHERE name="%s";' % name
                    cursor.execute(rollback)
                    self.cnx.commit()
                    cursor.close()
                    err = sys.exc_info()
                    print(err)
            elif visibility == 'public':
                add_corpus = 'INSERT INTO public_corpus VALUES("%s", "%s", "%r", "%s");' % (
                    name, language, self.bool_converter(is_diachronic), p_attributes)
                cursor.execute(add_corpus)

                update_sharing = 'INSERT INTO sharing VALUES("%s", "%s");' % (name, username)
                cursor.execute(update_sharing)

                self.cnx.commit()
                cursor.close()

                p_attr = p_attributes.split(',')

                try:
                    if is_diachronic:
                        DiachronicCorpus(name=name, username=username, visibility=visibility, language=language,
                                         p_attributes=p_attr)
                    else:
                        Corpus(name=name, visibility='public', language=language, p_attributes=p_attr)
                    is_added = not is_added
                except:
                    cursor = self.cnx.cursor()
                    rollback_sharing = 'DELETE FROM sharing WHERE corpus_name="%s" AND username="%s";' % (
                        name, username)
                    rollback_public_corpus = 'DELETE FROM public_corpus WHERE name="%s";' % name
                    cursor.execute(rollback_public_corpus)
                    cursor.execute(rollback_sharing)
                    self.cnx.commit()
                    cursor.close()
                    print('Something went wrong during corpus creation. Operation rolled back.')
            else:
                print('Invalid visibility!')
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return is_added

    def delete_corpus(self, corpus_name, username):
        is_deleted = False
        try:
            cursor = self.cnx.cursor(buffered=True)
            is_guest = self.isguest(corpus_name, cursor, username)

            is_owner = self.isowner(corpus_name, cursor, username)

            is_public_corpus = self.ispublic(corpus_name, cursor)

            if is_guest == 1:
                is_diachronic, language, owner, p_attributes, mode = self.get_guest_parameters(corpus_name, cursor,
                                                                                               username)
                if mode == 'w':
                    delete_private = 'DELETE FROM private_corpus WHERE name="%s" AND username="%s";' % (
                        corpus_name, owner)
                    cursor.execute(delete_private)
                    self.cnx.commit()
                    if is_diachronic == 1:
                        p_attr = p_attributes.split(',')
                        c = DiachronicCorpus(name=corpus_name, username=owner, visibility='private', language=language,
                                             p_attributes=p_attr)
                        c.delete()
                        is_deleted = True
                    elif is_diachronic == 0:
                        p_attr = p_attributes.split(',')
                        c = Corpus(name=corpus_name, username=owner, visibility='private', language=language,
                                   p_attributes=p_attr)
                        c.delete()
                        is_deleted = True
                    else:
                        raise Exception('You have not permission to modify this corpus')
                else:
                    print('Invalid corpus type!')
            elif is_owner == 1:
                is_diachronic, language, p_attributes = self.get_owner_parameters(corpus_name, cursor, username)
                delete_private = 'DELETE FROM private_corpus WHERE name="%s" AND username="%s";' % (
                    corpus_name, username)
                cursor.execute(delete_private)
                self.cnx.commit()
                if is_diachronic == 1:
                    p_attr = p_attributes.split(',')
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='private', language=language,
                                         p_attributes=p_attr)
                    c.delete()
                    is_deleted = True
                elif is_diachronic == 0:
                    p_attr = p_attributes.split(',')
                    c = Corpus(name=corpus_name, username=username, visibility='private', language=language,
                               p_attributes=p_attr)
                    c.delete()
                    is_deleted = True
                else:
                    raise Exception('Invalid corpus type!')
            elif is_public_corpus == 1:
                is_diachronic, language, p_attr = self.get_public_parameters(corpus_name, cursor)
                delete_public = 'DELETE FROM public_corpus WHERE name="%s";' % corpus_name
                cursor.execute(delete_public)
                self.cnx.commit()
                if is_diachronic == 1:
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='public', language=language,
                                         p_attributes=p_attr)
                    c.delete()
                    is_deleted = True
                elif is_diachronic == 0:
                    c = Corpus(name=corpus_name, username=username, visibility='public', language=language,
                               p_attributes=p_attr)
                    c.delete()
                    is_deleted = True
                else:
                    raise Exception('Invalid corpus type!')
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return is_deleted

    def view_corpus(self, name, visibility, username=None):
        information = 'No information'
        try:
            if visibility == 'private':
                select_language = 'SELECT language FROM private_corpus WHERE name="%s" AND username="%s";' % (
                    name, username)
                cursor = self.cnx.cursor()
                cursor.execute(select_language)
                language = cursor.fetchone()[0]
                self.cnx.commit()
                cursor.close()
                c = Corpus(name=name, username=username, visibility='private', language=language)
                information = c.describe()
            elif visibility == 'public':
                select_language = 'SELECT language FROM public_corpus WHERE name="%s";' % name
                cursor = self.cnx.cursor()
                cursor.execute(select_language)
                language = cursor.fetchone()[0]
                self.cnx.commit()
                cursor.close()
                c = Corpus(name=name, visibility='public', language=language)
                information = c.describe()
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return information

    def list_corpus(self, username):
        corpora = []
        try:
            show_private_corpus = 'SELECT name FROM private_corpus WHERE username="%s";' % username
            show_public_corpus = 'SELECT name FROM public_corpus, sharing WHERE username="%s";' % username
            cursor = self.cnx.cursor()
            cursor.execute(show_private_corpus)
            private_corpora = cursor.fetchall()
            cursor.execute(show_public_corpus)
            public_corpora = cursor.fetchall()
            corpora.extend(private_corpora)
            corpora.extend(public_corpora)
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return corpora

    def share_corpus(self, owner_username, guest_username, corpus_name, mode):
        shared = False
        try:
            if mode == 'w' or mode == 'r':
                share_corpus = 'INSERT INTO granting_access VALUES("%s", "%s", "%s", \'%c\');' % (
                    owner_username, corpus_name, guest_username, mode)
                cursor = self.cnx.cursor()
                cursor.execute(share_corpus)
                self.cnx.commit()
                cursor.close()
                shared = not shared
            else:
                raise Exception('Mode is not valid!')
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return shared

    def replace_doc_in_corpus(self, corpus_name, username, old_document, new_document):
        is_replaced = False
        try:
            cursor = self.cnx.cursor(buffered=True)
            is_guest = self.isguest(corpus_name, cursor, username)

            is_owner = self.isowner(corpus_name, cursor, username)

            is_public_corpus = self.ispublic(corpus_name, cursor)

            if is_guest == 1:
                is_diachronic, language, owner, p_attributes, mode = self.get_guest_parameters(corpus_name, cursor,
                                                                                               username)
                if mode == 'w':
                    if is_diachronic == 1:
                        p_attr = p_attributes.split(',')
                        c = DiachronicCorpus(name=corpus_name, username=owner, visibility='private', language=language,
                                             p_attributes=p_attr)
                        c.replace_document(old_doc=old_document, new_doc=new_document)
                        is_replaced = not is_replaced
                    elif is_diachronic == 0:
                        p_attr = p_attributes.split(',')
                        c = Corpus(name=corpus_name, username=owner, visibility='private', language=language,
                                   p_attributes=p_attr)
                        c.replace_document(old_doc=old_document, new_doc=new_document)
                        is_replaced = not is_replaced
                    else:
                        raise Exception('Invalid corpus type')
                else:
                    raise Exception('You have no permission to modify this corpus')
            elif is_owner == 1:
                is_diachronic, language, p_attributes = self.get_owner_parameters(corpus_name, cursor, username)
                if is_diachronic == 1:
                    p_attr = p_attributes.split(',')
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='private', language=language,
                                         p_attributes=p_attr)
                    c.replace_document(old_doc=old_document, new_doc=new_document)
                    is_replaced = True
                elif is_diachronic == 0:
                    p_attr = p_attributes.split(',')
                    c = Corpus(name=corpus_name, username=username, visibility='private', language=language,
                               p_attributes=p_attr)
                    c.replace_document(old_doc=old_document, new_doc=new_document)
                    is_replaced = True
                else:
                    raise Exception('Invalid corpus type!')
            elif is_public_corpus == 1:
                is_diachronic, language, p_attr = self.get_public_parameters(corpus_name, cursor)
                if is_diachronic == 1:
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='public', language=language,
                                         p_attributes=p_attr)
                    c.replace_document(old_doc=old_document, new_doc=new_document)
                    is_replaced = True
                elif is_diachronic == 0:
                    c = Corpus(name=corpus_name, username=username, visibility='public', language=language,
                               p_attributes=p_attr)
                    c.replace_document(old_doc=old_document, new_doc=new_document)
                    is_replaced = True
                else:
                    raise Exception('Invalid corpus type!')
            else:
                raise Exception('You have no permission to write on this corpus or the corpus does not exist!')

        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return is_replaced

    def add_doc_to_corpus(self, corpus_name, username, document):
        is_added = False
        try:
            cursor = self.cnx.cursor(buffered=True)
            # guest add document
            is_guest = self.isguest(corpus_name, cursor, username)

            # owner add document
            is_owner = self.isowner(corpus_name, cursor, username)

            # all users can add a document
            is_public_corpus = self.ispublic(corpus_name, cursor)

            if is_guest == 1:
                is_diachronic, language, owner, p_attributes, mode = self.get_guest_parameters(corpus_name, cursor,
                                                                                               username)
                if mode == 'w':
                    if is_diachronic == 1:
                        p_attr = p_attributes.split(',')
                        c = DiachronicCorpus(name=corpus_name, username=owner, visibility='private', language=language,
                                             p_attributes=p_attr)
                        c.add_document(document)
                        is_added = not is_added
                    elif is_diachronic == 0:
                        p_attr = p_attributes.split(',')
                        c = Corpus(name=corpus_name, username=owner, visibility='private', language=language,
                                   p_attributes=p_attr)
                        c.add_document(document)
                        is_added = not is_added
                    else:
                        raise Exception('Invalid corpus type!')
                else:
                    raise Exception('You have no permission to modify this corpus')
            elif is_owner == 1:
                is_diachronic, language, p_attributes = self.get_owner_parameters(corpus_name, cursor, username)
                if is_diachronic == 1:
                    p_attr = p_attributes.split(',')
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='private', language=language,
                                         p_attributes=p_attr)
                    c.add_document(document)
                    is_added = not is_added
                elif is_diachronic == 0:
                    p_attr = p_attributes.split(',')
                    c = Corpus(name=corpus_name, username=username, visibility='private', language=language,
                               p_attributes=p_attr)
                    c.add_document(document)
                    is_added = not is_added
                else:
                    print('Invalid corpus type!')
            elif is_public_corpus == 1:
                is_diachronic, language, p_attr = self.get_public_parameters(corpus_name, cursor)
                if is_diachronic == 1:
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='public', language=language,
                                         p_attributes=p_attr)
                    c.add_document(document)
                    is_added = not is_added
                elif is_diachronic == 0:
                    c = Corpus(name=corpus_name, username=username, visibility='public', language=language,
                               p_attributes=p_attr)
                    c.add_document(document)
                    is_added = not is_added
                else:
                    print('Invalid corpus type!')
            else:
                print('You have no permission to write on this corpus or the corpus does not exist!')

            cursor.close()
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return is_added

    def delete_document_from_corpus(self, corpus_name, username, document):
        is_deleted = False
        try:
            # guest delete a document
            cursor = self.cnx.cursor(buffered=True)
            is_guest = self.isguest(corpus_name, cursor, username)

            # owner can delete a document
            is_owner = self.isowner(corpus_name, cursor, username)

            # all the users can delete a document
            is_public_corpus = self.ispublic(corpus_name, cursor)

            if is_guest == 1:
                is_diachronic, language, owner, p_attributes, mode = self.get_guest_parameters(corpus_name, cursor,
                                                                                               username)
                if mode == 'w':
                    if is_diachronic == 1:
                        p_attr = p_attributes.split(',')
                        c = DiachronicCorpus(name=corpus_name, username=owner, visibility='private', language=language,
                                             p_attributes=p_attr)
                        c.delete_document(document)
                        is_deleted = not is_deleted
                    elif is_diachronic == 0:
                        p_attr = p_attributes.split(',')
                        c = Corpus(name=corpus_name, username=owner, visibility='private', language=language,
                                   p_attributes=p_attr)
                        c.delete_document(document)
                        is_deleted = not is_deleted
                    else:
                        raise Exception('Invalid corpus type!')
                else:
                    raise Exception('You have no permission to modify this corpus')
            elif is_owner == 1:
                is_diachronic, language, p_attributes = self.get_owner_parameters(corpus_name, cursor, username)
                if is_diachronic == 1:
                    p_attr = p_attributes.split(',')
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='private', language=language,
                                         p_attributes=p_attr)
                    c.delete_document(document)
                    is_deleted = not is_deleted
                elif is_diachronic == 0:
                    p_attr = p_attributes.split(',')
                    c = Corpus(name=corpus_name, username=username, visibility='private', language=language,
                               p_attributes=p_attr)
                    c.delete_document(document)
                    is_deleted = not is_deleted
                else:
                    print('Invalid corpus type!')
            elif is_public_corpus == 1:
                is_diachronic, language, p_attr = self.get_public_parameters(corpus_name, cursor)
                if is_diachronic == 1:
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='public', language=language,
                                         p_attributes=p_attr)
                    c.delete_document(document)
                    is_deleted = not is_deleted
                elif is_diachronic == 0:
                    c = Corpus(name=corpus_name, username=username, visibility='public', language=language,
                               p_attributes=p_attr)
                    c.delete_document(document)
                    is_deleted = not is_deleted
                else:
                    print('Invalid corpus type!')
            else:
                print('You have no permission to modify this corpus or the corpus does not exist!')

            cursor.close()

        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return is_deleted

    def show_frequency_list(self, corpus_name, username, word, diachronic_attribute):
        global_frequencies = None
        try:
            cursor = self.cnx.cursor(buffered=True)
            is_guest = self.isguest(corpus_name, cursor, username)

            is_owner = self.isowner(corpus_name, cursor, username)

            is_public_corpus = self.ispublic(corpus_name, cursor)

            if is_guest == 1:
                is_diachronic, language, owner, p_attributes, mode = self.get_guest_parameters(corpus_name, cursor,
                                                                                               username)
                if mode == 'w' or mode == 'r':
                    if is_diachronic == 1:
                        p_attr = p_attributes.split(',')
                        c = DiachronicCorpus(name=corpus_name, username=owner, visibility='private', language=language,
                                             p_attributes=p_attr)
                        # global_frequencies = c.query(
                        #     ['Word = [word = "%s"];' % word, 'group Word matchend %s;' % diachronic_attribute])
                        global_frequencies = self.frequency_list_experiment(c, 'year')
                    else:
                        print('The corpus is not diachronic!')
                else:
                    raise Exception('You have no permission to modify this corpus')
            elif is_owner == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM private_corpus WHERE name="%s" AND username="%s";' % (
                    corpus_name, username)
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                if is_diachronic == 1:
                    p_attr = p_attributes.split(',')
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='private', language=language,
                                         p_attributes=p_attr)
                    global_frequencies = c.query(
                        ['A = [word = "%s"];' % word, 'group A matchend %s;' % diachronic_attribute])
                    # global_frequencies = self.frequency_list_experiment(c, 'year')
                else:
                    print('The corpus is not diachronic!')
            elif is_public_corpus == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM public_corpus WHERE name="%s"' % corpus_name
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                p_attr = p_attributes.split(',')
                if is_diachronic == 1:
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='public', language=language,
                                         p_attributes=p_attr)
                    global_frequencies = c.query(
                        ['Word = [word = "%s"];' % word, 'group Word matchend %s;' % corpus_name])
                else:
                    print('Invalid corpus type!')

            cursor.close()
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return global_frequencies

    def frequency_list_experiment(self, corpus, diachronic_attribute):
        frequency_lists = {}
        with open(corpus.corpus_path + os.sep + 'dictionary.txt', 'r') as f:
            for word in f.readlines():
                word = word.split()[0]
                word_frequencies = None
                try:
                    word_frequencies = corpus.query(['A = "%s";' % word, 'group A matchend %s;' % diachronic_attribute])
                except:
                    print('cazzi')
                word_frequencies = str(word_frequencies).split()
                if len(word_frequencies) == 4:
                    difference = int(word_frequencies[3]) - int(word_frequencies[1])
                    frequency_lists.update({word: difference})

        return frequency_lists

    def show_period_frequence_list(self, corpus_name, username, word, diachronic_attribute, range):
        periodic_frequencies = None
        try:
            # guest delete a document
            cursor = self.cnx.cursor(buffered=True)
            is_guest = self.isguest(corpus_name, cursor, username)

            # owner can delete a document
            is_owner = self.isowner(corpus_name, cursor, username)

            # all the users can delete a document
            is_public_corpus = self.ispublic(corpus_name, cursor)

            if is_guest == 1:
                get_language_owner = 'SELECT p.language, p.is_diachronic, p.p_attributes, g.owner FROM private_corpus as p, granting_access as g WHERE corpus=name AND corpus="%s" AND guest="%s" AND (mode=\'r\' OR mode=\'w\');' % (
                    corpus_name, username)
                cursor.execute(get_language_owner)
                language, is_diachronic, p_attributes, owner = cursor.fetchone()
                if is_diachronic == 1:
                    p_attr = p_attributes.split(',')
                    c = DiachronicCorpus(name=corpus_name, username=owner, visibility='private', language=language,
                                         p_attributes=p_attr)
                    command = "A = "
                    for i in range:
                        command += '[%s="%s"] |' % (diachronic_attribute, i)
                    command += '"%s";' % word
                    periodic_frequencies = c.query(
                        [command, 'count A by %s;' % diachronic_attribute])
                else:
                    print('The corpus is not diachronic!')
            elif is_owner == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM private_corpus WHERE name="%s" AND username="%s";' % (
                    corpus_name, username)
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                if is_diachronic == 1:
                    p_attr = p_attributes.split(',')
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='private', language=language,
                                         p_attributes=p_attr)
                    command = "A = "
                    for i in range:
                        command += '[%s="%s"] ' % (diachronic_attribute, i)
                    command += '"%s";' % word
                    periodic_frequencies = c.query(
                        [command, 'count A by %s;' % diachronic_attribute])
                else:
                    print('The corpus is not diachronic!')
            elif is_public_corpus == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM public_corpus WHERE name="%s"' % corpus_name
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                p_attr = p_attributes.split(',')
                if is_diachronic == 1:
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='public', language=language,
                                         p_attributes=p_attr)
                    command = "A = "
                    for i in range:
                        command += '[%s="%s"] ' % (diachronic_attribute, i)
                    command += '"%s";' % word
                    periodic_frequencies = c.query(
                        [command, 'count A by %s;' % diachronic_attribute])
                else:
                    print('Invalid corpus type!')

            cursor.close()
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return periodic_frequencies

    def show_collocation(self, corpus_name, username, word, n_word_context, years_range=None):
        collocations = None
        get_left_context = 'tabulate A match[-%s]..match[-1] word;' % n_word_context
        get_right_context = 'tabulate A matchend[1]..matchend[%s] word;' % n_word_context
        try:
            cursor = self.cnx.cursor(buffered=True)
            is_guest = self.isguest(corpus_name, cursor, username)

            is_owner = self.isowner(corpus_name, cursor, username)

            is_public_corpus = self.ispublic(corpus_name, cursor)

            if is_guest == 1:
                is_diachronic, language, owner, p_attributes, mode = self.get_guest_parameters(corpus_name, cursor,
                                                                                               username)
                if mode == 'w' or mode == 'r':
                    if is_diachronic == 1:
                        p_attr = p_attributes.split(',')
                        c = DiachronicCorpus(name=corpus_name, username=owner, visibility='private', language=language,
                                             p_attributes=p_attr)
                        collocations = c.collocation_sketch_engine(get_left_context, get_right_context, word,
                                                                   years_range)
                        # compute_collocations(collocations, get_left_context, get_right_context, word,
                        #                                       years_range)
                    elif is_diachronic == 0:
                        p_attr = p_attributes.split(',')
                        c = Corpus(name=corpus_name, username=owner, visibility='private', language=language,
                                   p_attributes=p_attr)
                        collocations = c.compute_collocations(collocations, get_left_context, get_right_context, word)
                    else:
                        raise Exception('Invalid corpus type')
                else:
                    raise Exception('You have no permission to see this corpus')
            elif is_owner == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM private_corpus WHERE name="%s" AND username="%s";' % (
                    corpus_name, username)
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                if is_diachronic == 1:
                    p_attr = p_attributes.split(',')
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='private', language=language,
                                         p_attributes=p_attr)

                    collocations = c.collocation_sketch_engine(get_left_context, get_right_context, word, years_range)
                    # compute_collocations(collocations, get_left_context, get_right_context, word,
                    #                                   years_range)
                elif is_diachronic == 0:
                    p_attr = p_attributes.split(',')
                    c = Corpus(name=corpus_name, username=username, visibility='private', language=language,
                               p_attributes=p_attr)
                    collocations = c.compute_collocations(collocations, get_left_context, get_right_context, word)
                else:
                    raise Exception('Invalid corpus type')
            elif is_public_corpus == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM public_corpus WHERE name="%s"' % corpus_name
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                p_attr = p_attributes.split(',')
                if is_diachronic == 1:
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='public', language=language,
                                         p_attributes=p_attr)
                    collocations = c.compute_collocations(collocations, get_left_context, get_right_context, word,
                                                          years_range)
                elif is_diachronic == 0:
                    c = Corpus(name=corpus_name, username=username, visibility='private', language=language,
                               p_attributes=p_attr)
                    collocations = c.compute_collocations(collocations, get_left_context, get_right_context, word)
                else:
                    raise Exception('Invalid corpus type')
            cursor.close()
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return collocations

    def show_time_series(self, corpus_name, username, years_range, split):
        timeseries_cumulative = None
        timeseries_pointwise = None
        try:
            cursor = self.cnx.cursor(buffered=True)

            is_guest = self.isguest(corpus_name, cursor, username)

            is_owner = self.isowner(corpus_name, cursor, username)

            is_public_corpus = self.ispublic(corpus_name, cursor)

            if is_guest == 1:
                is_diachronic, language, owner, p_attributes, mode = self.get_guest_parameters(corpus_name, cursor,
                                                                                               username)
                if mode == 'w' or mode == 'r':
                    if is_diachronic == 1:
                        p_attr = p_attributes.split(',')
                        c = DiachronicCorpus(name=corpus_name, username=owner, visibility='private', language=language,
                                             p_attributes=p_attr)
                        ut = Utilities(corpus=c, conf_file='conf.txt')
                        timeseries_cumulative, timeseries_pointwise = c.timeseries(split, timeseries_cumulative,
                                                                                   timeseries_pointwise, ut,
                                                                                   years_range)
                    else:
                        raise Exception('The corpus is not diachronic!')
                else:
                    raise Exception('You have no permission to show timeseries')
            elif is_owner == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM private_corpus WHERE name="%s" AND username="%s";' % (
                    corpus_name, username)
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                if is_diachronic == 1:
                    p_attr = p_attributes.split(',')
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='private', language=language,
                                         p_attributes=p_attr)
                    ut = Utilities(corpus=c, conf_file='conf.txt')
                    timeseries_cumulative, timeseries_pointwise = c.timeseries(split, timeseries_cumulative,
                                                                               timeseries_pointwise, ut, years_range)
                else:
                    raise Exception('The corpus is not diachronic!')
            elif is_public_corpus == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM public_corpus WHERE name="%s"' % corpus_name
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                p_attr = p_attributes.split(',')
                if is_diachronic == 1:
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='public', language=language,
                                         p_attributes=p_attr)
                    ut = Utilities(corpus=c, conf_file='conf.txt')
                    timeseries_cumulative, timeseries_pointwise = c.timeseries(split,
                                                                               timeseries_cumulative,
                                                                               timeseries_pointwise, ut, years_range)
                else:
                    raise Exception('The corpus is not diachronic!')

            cursor.close()
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return self.format_timeseries(timeseries_cumulative), self.format_timeseries(timeseries_pointwise)

    def detect_change_points(self, corpus_name, username, years_range, split):
        change_points_cumulative = None
        change_points_pointwise = None
        try:
            cursor = self.cnx.cursor(buffered=True)
            is_guest = self.isguest(corpus_name, cursor, username)

            is_owner = self.isowner(corpus_name, cursor, username)

            is_public_corpus = self.ispublic(corpus_name, cursor)

            if is_guest == 1:
                is_diachronic, language, owner, p_attributes, mode = self.get_guest_parameters(corpus_name, cursor,
                                                                                               username)
                if mode == 'w' or mode == 'r':
                    if is_diachronic == 1:
                        p_attr = p_attributes.split(',')
                        c = DiachronicCorpus(name=corpus_name, username=owner, visibility='private', language=language,
                                             p_attributes=p_attr)
                        ut = Utilities(corpus=c, conf_file='conf.txt')
                        timeseries_cumulative = None
                        timeseries_pointwise = None
                        timeseries_cumulative, timeseries_pointwise = c.timeseries(split, timeseries_cumulative,
                                                                                   timeseries_pointwise, ut,
                                                                                   years_range)
                        change_points_cumulative = ut.computeCPD(timeseries_cumulative)
                        change_points_pointwise = ut.computeCPD(timeseries_pointwise)
                    else:
                        raise Exception('The corpus is not diachronic!')
                else:
                    raise Exception('You have no permission to see change points')
            elif is_owner == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM private_corpus WHERE name="%s" AND username="%s";' % (
                    corpus_name, username)
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                if is_diachronic == 1:
                    p_attr = p_attributes.split(',')
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='private', language=language,
                                         p_attributes=p_attr)
                    ut = Utilities(corpus=c, conf_file='conf.txt')
                    timeseries_cumulative = None
                    timeseries_pointwise = None
                    timeseries_cumulative, timeseries_pointwise = c.timeseries(split, timeseries_cumulative,
                                                                               timeseries_pointwise, ut, years_range)
                    change_points_cumulative = ut.change_points_experimentation(timeseries_cumulative)
                    change_points_pointwise = ut.change_points_experimentation(timeseries_pointwise)
                else:
                    raise Exception('The corpus is not diachronic!')
            elif is_public_corpus == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM public_corpus WHERE name="%s"' % corpus_name
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                p_attr = p_attributes.split(',')
                if is_diachronic == 1:
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='public', language=language,
                                         p_attributes=p_attr)
                    ut = Utilities(corpus=c, conf_file='conf.txt')
                    timeseries_cumulative = None
                    timeseries_pointwise = None
                    timeseries_cumulative, timeseries_pointwise = c.timeseries(split, timeseries_cumulative,
                                                                               timeseries_pointwise, ut, years_range)
                    change_points_cumulative = ut.computeCPD(timeseries_cumulative)
                    change_points_pointwise = ut.computeCPD(timeseries_pointwise)
                else:
                    raise Exception('The corpus is not diachronic!')
            else:
                raise Exception('The corpus is not diachronic!')

            cursor.close()
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return change_points_cumulative, change_points_pointwise

    def write_cqp_query(self, corpus_name, username, commands):
        result = None
        try:
            cursor = self.cnx.cursor(buffered=True)
            is_guest = self.isguest(corpus_name, cursor, username)

            is_owner = self.isowner(corpus_name, cursor, username)

            is_public_corpus = self.ispublic(corpus_name, cursor)

            if is_guest == 1:
                is_diachronic, language, owner, p_attributes, mode = self.get_guest_parameters(corpus_name, cursor,
                                                                                               username)
                if mode == 'w' or mode == 'r':
                    if is_diachronic == 1:
                        p_attr = p_attributes.split(',')
                        c = DiachronicCorpus(name=corpus_name, username=owner, visibility='private',
                                             language=language,
                                             p_attributes=p_attr)
                        result = c.query(commands)
                    else:
                        p_attr = p_attributes.split(',')
                        c = Corpus(name=corpus_name, username=owner, visibility='private', language=language,
                                   p_attributes=p_attr)
                        result = c.query(commands)
                else:
                    raise Exception('You have no permission to see concordances')
            elif is_owner == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM private_corpus WHERE name="%s" AND username="%s";' % (
                    corpus_name, username)
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                if is_diachronic == 1:
                    p_attr = p_attributes.split(',')
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='private', language=language,
                                         p_attributes=p_attr)
                    result = c.query(commands)
                else:
                    p_attr = p_attributes.split(',')
                    c = Corpus(name=corpus_name, username=username, visibility='private', language=language,
                               p_attributes=p_attr)
                    result = c.query(commands)
            elif is_public_corpus == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM public_corpus WHERE name="%s"' % corpus_name
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                p_attr = p_attributes.split(',')
                if is_diachronic == 1:
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='public', language=language,
                                         p_attributes=p_attr)
                    result = c.query(commands)
                else:
                    c = Corpus(name=corpus_name, username=username, visibility='private', language=language,
                               p_attributes=p_attr)
                    c.query(commands)
            else:
                raise Exception('Invalid ')

        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)

        return result

    def show_concordances(self, corpus_name, username, word, n_word_context, years_range=None):
        concordances = ''
        get_contexts = 'tabulate A match[-%s]..match[-1] word, match word, matchend[1]..matchend[%s] word;' % (
            n_word_context, n_word_context)
        try:
            cursor = self.cnx.cursor(buffered=True)
            is_guest = self.isguest(corpus_name, cursor, username)

            is_owner = self.isowner(corpus_name, cursor, username)

            is_public_corpus = self.ispublic(corpus_name, cursor)

            if is_guest == 1:
                is_diachronic, language, owner, p_attributes, mode = self.get_guest_parameters(corpus_name, cursor,
                                                                                               username)
                if mode == 'w' or mode == 'r':
                    if is_diachronic == 1:
                        p_attr = p_attributes.split(',')
                        c = DiachronicCorpus(name=corpus_name, username=owner, visibility='private',
                                             language=language,
                                             p_attributes=p_attr)
                        concordances = c.get_concordances(concordances, get_contexts, word, years_range)
                    else:
                        p_attr = p_attributes.split(',')
                        c = Corpus(name=corpus_name, username=owner, visibility='private', language=language,
                                   p_attributes=p_attr)
                        concordances = c.get_concordances(concordances, get_contexts, word)
                else:
                    raise Exception('You have no permission to see concordances')
            elif is_owner == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM private_corpus WHERE name="%s" AND username="%s";' % (
                    corpus_name, username)
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                if is_diachronic == 1:
                    p_attr = p_attributes.split(',')
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='private', language=language,
                                         p_attributes=p_attr)
                    concordances = c.get_concordances(concordances, get_contexts, word, years_range)
                else:
                    p_attr = p_attributes.split(',')
                    c = Corpus(name=corpus_name, username=username, visibility='private', language=language,
                               p_attributes=p_attr)
                    concordances = c.get_concordances(concordances, get_contexts, word)
            elif is_public_corpus == 1:
                get_language = 'SELECT language, is_diachronic, p_attributes FROM public_corpus WHERE name="%s"' % corpus_name
                cursor.execute(get_language)
                language, is_diachronic, p_attributes = cursor.fetchone()
                p_attr = p_attributes.split(',')
                if is_diachronic == 1:
                    c = DiachronicCorpus(name=corpus_name, username=username, visibility='public', language=language,
                                         p_attributes=p_attr)
                    concordances = c.get_concordances(concordances, get_contexts, word, years_range)
                else:
                    c = Corpus(name=corpus_name, username=username, visibility='private', language=language,
                               p_attributes=p_attr)
                    concordances = c.get_concordances(concordances, get_contexts, word)
            else:
                raise Exception('Invalid ')
        except mysql.connector.Error as err:
            print('Something went wrong: %s' % err)
        return concordances

    def get_owner_parameters(self, corpus_name, cursor, username):
        get_language = 'SELECT language, is_diachronic, p_attributes FROM private_corpus WHERE name="%s" AND username="%s";' % (
            corpus_name, username)
        cursor.execute(get_language)
        language, is_diachronic, p_attributes = cursor.fetchone()
        return is_diachronic, language, p_attributes

    def get_public_parameters(self, corpus_name, cursor):
        get_language = 'SELECT language, is_diachronic, p_attributes FROM public_corpus WHERE name="%s"' % corpus_name
        cursor.execute(get_language)
        language, is_diachronic, p_attributes = cursor.fetchone()
        p_attr = p_attributes.split(',')
        return is_diachronic, language, p_attr

    def get_guest_parameters(self, corpus_name, cursor, username):
        get_language_owner = 'SELECT p.language, p.is_diachronic, p.p_attributes, g.owner, g.mode FROM private_corpus as p, granting_access as g WHERE name=corpus AND corpus="%s" AND guest="%s";' % (
            corpus_name, username)
        cursor.execute(get_language_owner)
        language, is_diachronic, p_attributes, owner, mode = cursor.fetchone()
        return is_diachronic, language, owner, p_attributes, mode

    def isguest(self, corpus_name, cursor, username):
        check_guest = 'SELECT EXISTS(SELECT p.language, g.owner FROM private_corpus as p, granting_access as g WHERE corpus="%s" AND guest="%s" AND (mode=\'r\' OR mode=\'w\'));' % (
            corpus_name, username)
        cursor.execute(check_guest)
        is_guest = cursor.fetchone()[0]
        self.cnx.commit()
        return is_guest

    def ispublic(self, corpus_name, cursor):
        check_public_corpus = 'SELECT EXISTS(SELECT language FROM public_corpus WHERE name="%s");' % corpus_name
        cursor.execute(check_public_corpus)
        is_public_corpus = cursor.fetchone()[0]
        self.cnx.commit()
        return is_public_corpus

    def isowner(self, corpus_name, cursor, username):
        check_owner = 'SELECT EXISTS(SELECT language FROM private_corpus WHERE name="%s" AND username="%s");' % (
            corpus_name, username)
        cursor.execute(check_owner)
        is_owner = cursor.fetchone()[0]
        self.cnx.commit()
        return is_owner

    def format_collocations(self, collocations):
        output = ''
        for word, std_dev in collocations:
            output += '%s: %s\n' % (word, std_dev)

        return output

    def format_timeseries(self, timeseries):
        output = ''
        for r in range(timeseries.shape[0]):
            output += '%s|%s\n' % (r, list(timeseries[r]))
        return output

    def none_converter(self, input):
        result = input
        if input is None:
            result = 'NULL'
        return result

    def bool_converter(self, input):
        result = input
        if input:
            result = 1
        elif not input:
            result = 0
        return result

    def terminate(self):
        self.cnx.close()


if __name__ == '__main__':
    dbo = DatabaseOperation()

    # dbo.sign_up(name='Paolo', surname='Rossi', username='wolf_95', date_of_birth='1995-1-3', password='admin', question='quanti anni ho?', answer='25')

    # dbo.delete_account(username='wolf_95', password='admin')

    # dbo.add_corpus(name='LatinCorpus', language='latin-perseus', is_diachronic=True,
    #                p_attributes='pos,lemma,tag,dep,year',
    #                username='wolf_95', visibility='private')

    # dbo.share_corpus(owner_username='Loop', guest_username='wolf_95', corpus_name='Sherlock', mode='w')

    # dbo.unshare_corpus(owner='Loop', corpus='Sherlock', guest='wolf')

    # print(dbo.list_corpus('Loop'))

    # dbo.modify_permission(owner='Loop', corpus='Bigram', guest='vale_tina', new_permission='r')

    # dbo.delete_corpus(corpus_name='LatinCorpus', username='wolf_95')

    # dbo.add_doc_to_corpus(corpus_name='Sherlock', username='wolf_95', document='Sherlock.txt')

    # print(dbo.show_frequency_list(corpus_name='EnglishCorpus', username='Loop', word='player', diachronic_attribute='year'))

    # dbo.delete_document_from_corpus(corpus_name='Sherlock', username='wolf', document='Sherlock.txt')
    # collocations = dbo.show_collocation(corpus_name='EnglishCorpus', word='bag', username='Loop', n_word_context=6,
    #                                     years_range=range(2, 3))
    #
    # print(collocations)
    # dbo.show_time_series(corpus_name='LatinCorpus', username='Loop', years_range=range(1, 3), split=1)

    cum, poi = dbo.detect_change_points(corpus_name='SwedishCorpus', username='Loop', years_range=range(21, 23), split=1)
    print('cumulative: ', cum)
    print('point-wise: ', poi)

    # print(dbo.show_concordances('EnglishCorpus', 'Loop', 'player', 20, range(2, 3)))


    dbo.terminate()
