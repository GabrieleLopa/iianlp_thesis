from flask import Flask

from db_connection import DatabaseOperation

app = Flask(__name__)


@app.route('/')
def index():
    return 'Hello, World!'


@app.route('/concordances')
def show_concordances():
    dbo = DatabaseOperation()
    concordance = dbo.show_concordances('Gutenberg', 'Loop', 'avevano', 20, range(1900, 1905))
    dbo.terminate()
    return concordance


@app.route('/collocations')
def show_collocations():
    dbo = DatabaseOperation()
    collocations = dbo.show_collocation(corpus_name='Gutenberg', word='contadino', username='Loop', n_word_context=4)
    dbo.terminate()
    return collocations


@app.route('/timeseries')
def show_timeseries():
    output = 'CUMULATIVE\n\n'
    dbo = DatabaseOperation()
    timeseries_cumulative, timeseries_pointwise = dbo.show_time_series(corpus_name='Gutenberg', username='Loop', years_range=range(1800, 2000), split=1)
    dbo.terminate()

    output += timeseries_cumulative + '\n\n POINTWISE\n\n'

    output += timeseries_pointwise

    return output


# @app.route('/changepoints')
# def show_change_points():


if __name__ == "__main__":
    app.run(debug=True)
