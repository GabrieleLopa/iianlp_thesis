import gzip
import multiprocessing
import os
import time
from multiprocessing import Pool
from threading import Thread
from threading import Lock
import tarfile
import spacy_udpipe
import xml.etree.ElementTree as ET
from gensim.utils import simple_preprocess
from numpy import unicode

import corpus

lock = Lock()


class XMLHandlerThread(Thread):
    def __init__(self, queue):
        Thread.__init__(self)
        self.result = []
        self.queue = queue

    def run(self):
        while not self.queue.empty():
            corpus, member, archive = self.queue.get()
            self.process(corpus, member, archive)
            self.queue.task_done()

    def process(self, corpus, member, archive):
            time.sleep(1)
            print('Processing: %s' % member.name)
            xml_handler = XMLHandler(corpus)
            # print('Loading document for %s' % member.name)
            xml_handler.load_doc(member, archive)
            # print('Document loaded for %s' % member.name)
            # print('Starting to write on file for %s' % member.name)
            # # lock.acquire()
            # xml_handler.write_on_file(doc, doc_name, year)
            # lock.release()
            print('Process finishes for %s' % member.name)


class XMLHandler:
    models_dic = {'estonian-ewt': 'estonian-ewt-ud-2.5-191206.udpipe',
                  'turkish-imst': 'turkish-imst-ud-2.5-191206.udpipe',
                  'finnish-ftb': 'finnish-ftb-ud-2.5-191206.udpipe',
                  'lithuanian-alksnis': 'lithuanian-alksnis-ud-2.5-191206.udpipe',
                  'english-partut': 'english-partut-ud-2.5-191206.udpipe',
                  'belarusian-hse': 'belarusian-hse-ud-2.5-191206.udpipe',
                  'hungarian-szeged': 'hungarian-szeged-ud-2.5-191206.udpipe',
                  'chinese-gsdsimp': 'chinese-gsdsimp-ud-2.5-191206.udpipe',
                  'urdu-udtb': 'urdu-udtb-ud-2.5-191206.udpipe',
                  'italian-twittiro': 'italian-twittiro-ud-2.5-191206.udpipe',
                  'estonian-edt': 'estonian-edt-ud-2.5-191206.udpipe',
                  'maltese-mudt': 'maltese-mudt-ud-2.5-191206.udpipe',
                  'coptic-scriptorium': 'coptic-scriptorium-ud-2.5-191206.udpipe',
                  'german-gsd': 'german-gsd-ud-2.5-191206.udpipe',
                  'chinese-gsd': 'chinese-gsd-ud-2.5-191206.udpipe',
                  'norwegian-bokmaal': 'norwegian-bokmaal-ud-2.5-191206.udpipe',
                  'lithuanian-hse': 'lithuanian-hse-ud-2.5-191206.udpipe',
                  'hebrew-htb': 'hebrew-htb-ud-2.5-191206.udpipe',
                  'serbian-set': 'serbian-set-ud-2.5-191206.udpipe',
                  'portuguese-bosque': 'portuguese-bosque-ud-2.5-191206.udpipe',
                  'korean-gsd': 'korean-gsd-ud-2.5-191206.udpipe',
                  'old_french-srcmf': 'old_french-srcmf-ud-2.5-191206.udpipe',
                  'romanian-rrt': 'romanian-rrt-ud-2.5-191206.udpipe',
                  'czech-cltt': 'czech-cltt-ud-2.5-191206.udpipe',
                  'norwegian-nynorsk': 'norwegian-nynorsk-ud-2.5-191206.udpipe',
                  'slovak-snk': 'slovak-snk-ud-2.5-191206.udpipe',
                  'finnish-tdt': 'finnish-tdt-ud-2.5-191206.udpipe',
                  'english-gum': 'english-gum-ud-2.5-191206.udpipe',
                  'hindi-hdtb': 'hindi-hdtb-ud-2.5-191206.udpipe',
                  'armenian-armtdp': 'armenian-armtdp-ud-2.5-191206.udpipe',
                  'norwegian-nynorsklia': 'norwegian-nynorsklia-ud-2.5-191206.udpipe',
                  'latvian-lvtb': 'latvian-lvtb-ud-2.5-191206.udpipe',
                  'bulgarian-btb': 'bulgarian-btb-ud-2.5-191206.udpipe',
                  'english-lines': 'english-lines-ud-2.5-191206.udpipe',
                  'english-ewt': 'english-ewt-ud-2.5-191206.udpipe',
                  'galician-ctg': 'galician-ctg-ud-2.5-191206.udpipe',
                  'dutch-alpino': 'dutch-alpino-ud-2.5-191206.udpipe',
                  'old_russian-torot': 'old_russian-torot-ud-2.5-191206.udpipe',
                  'vietnamese-vtb': 'vietnamese-vtb-ud-2.5-191206.udpipe',
                  'french-partut': 'french-partut-ud-2.5-191206.udpipe',
                  'classical_chinese-kyoto': 'classical_chinese-kyoto-ud-2.5-191206.udpipe',
                  'portuguese-gsd': 'portuguese-gsd-ud-2.5-191206.udpipe',
                  'korean-kaist': 'korean-kaist-ud-2.5-191206.udpipe',
                  'galician-treegal': 'galician-treegal-ud-2.5-191206.udpipe',
                  'slovenian-sst': 'slovenian-sst-ud-2.5-191206.udpipe',
                  'czech-fictree': 'czech-fictree-ud-2.5-191206.udpipe',
                  'russian-taiga': 'russian-taiga-ud-2.5-191206.udpipe',
                  'tamil-ttb': 'tamil-ttb-ud-2.5-191206.udpipe',
                  'japanese-gsd': 'japanese-gsd-ud-2.5-191206.udpipe',
                  'persian-seraji': 'persian-seraji-ud-2.5-191206.udpipe',
                  'ukrainian-iu': 'ukrainian-iu-ud-2.5-191206.udpipe',
                  'french-gsd': 'french-gsd-ud-2.5-191206.udpipe',
                  'latin-perseus': 'latin-perseus-ud-2.5-191206.udpipe',
                  'dutch-lassysmall': 'dutch-lassysmall-ud-2.5-191206.udpipe',
                  'croatian-set': 'croatian-set-ud-2.5-191206.udpipe',
                  'italian-partut': 'italian-partut-ud-2.5-191206.udpipe',
                  'irish-idt': 'irish-idt-ud-2.5-191206.udpipe',
                  'spanish-ancora': 'spanish-ancora-ud-2.5-191206.udpipe',
                  'czech-cac': 'czech-cac-ud-2.5-191206.udpipe',
                  'uyghur-udt': 'uyghur-udt-ud-2.5-191206.udpipe',
                  'gothic-proiel': 'gothic-proiel-ud-2.5-191206.udpipe',
                  'spanish-gsd': 'spanish-gsd-ud-2.5-191206.udpipe',
                  'old_church_slavonic-proiel': 'old_church_slavonic-proiel-ud-2.5-191206.udpipe',
                  'polish-pdb': 'polish-pdb-ud-2.5-191206.udpipe',
                  'french-sequoia': 'french-sequoia-ud-2.5-191206.udpipe',
                  'romanian-nonstandard': 'romanian-nonstandard-ud-2.5-191206.udpipe',
                  'italian-vit': 'italian-vit-ud-2.5-191206.udpipe',
                  'north_sami-giella': 'north_sami-giella-ud-2.5-191206.udpipe',
                  'marathi-ufal': 'marathi-ufal-ud-2.5-191206.udpipe',
                  'greek-gdt': 'greek-gdt-ud-2.5-191206.udpipe',
                  'wolof-wtb': 'wolof-wtb-ud-2.5-191206.udpipe',
                  'latin-ittb': 'latin-ittb-ud-2.5-191206.udpipe',
                  'scottish_gaelic-arcosg': 'scottish_gaelic-arcosg-ud-2.5-191206.udpipe',
                  'italian-postwita': 'italian-postwita-ud-2.5-191206.udpipe',
                  'russian-gsd': 'russian-gsd-ud-2.5-191206.udpipe',
                  'arabic-padt': 'arabic-padt-ud-2.        # Thread.__init__(self)5-191206.udpipe',
                  'german-hdt': 'german-hdt-ud-2.5-191206.udpipe',
                  'ancient_greek-perseus': 'ancient_greek-perseus-ud-2.5-191206.udpipe',
                  'italian-isdt': 'italian-isdt-ud-2.5-191206.udpipe',
                  'slovenian-ssj': 'slovenian-ssj-ud-2.5-191206.udpipe',
                  'telugu-mtg': 'telugu-mtg-ud-2.5-191206.udpipe',
                  'polish-lfg': 'polish-lfg-ud-2.5-191206.udpipe',
                  'ancient_greek-proiel': 'ancient_greek-proiel-ud-2.5-191206.udpipe',
                  'swedish-lines': 'swedish-lines-ud-2.5-191206.udpipe',
                  'czech-pdt': 'czech-pdt-ud-2.5-191206.udpipe',
                  'catalan-ancora': 'catalan-ancora-ud-2.5-191206.udpipe',
                  'latin-proiel': 'latin-proiel-ud-2.5-191206.udpipe',
                  'swedish-talbanken': 'swedish-talbanken-ud-2.5-191206.udpipe',
                  'afrikaans-afribooms': 'afrikaans-afribooms-ud-2.5-191206.udpipe',
                  'indonesian-gsd': 'indonesian-gsd-ud-2.5-191206.udpipe',
                  'basque-bdt': 'basque-bdt-ud-2.5-191206.udpipe',
                  'russian-syntagrus': 'russian-syntagrus-ud-2.5-191206.udpipe',
                  'french-spoken': 'french-spoken-ud-2.5-191206.udpipe',
                  'danish-ddt': 'danish-ddt-ud-2.5-191206.udpipe'}

    def __init__(self, corpus):
        self.corpus = corpus
        self.model = spacy_udpipe.load_from_path(lang=self.corpus.language,
                                                 path='udpipe_models' + os.sep + XMLHandler.models_dic[
                                                     self.corpus.language], disable=["parser", "textcat", "ner", "entity_linker", "entity_ruler", "merge_noun_chunks",
                                                    "merge_entities", "merge_subtokens"])
        self.model.max_length = 50000000

    def words_per_sentence(self, input_path):
        dict = {}
        text = ''
        input = open(self.corpus.documents_path + os.path.sep + input_path, 'r+')

        for line in input.readlines():
            text += line

        input.close()

        doc = self.model(text)

        for sentence in doc.sents:
            words = [(token.text, token.pos_, token.lemma_) for token in sentence]
            dict.update({str(sentence): words})

        self.dict = dict

        return dict

    def load_doc(self, member, archive):
        doc_name = str(member.name).split('/')[len(str(member.name).split('/')) - 1]
        diac = self.diachronic_attribute(str(doc_name))
        for chunk in self.read_document(archive, member):
            try:

                doc = self.model(unicode(chunk, encoding='utf-8', errors='ignore'), disable=["textcat", "ner", "entity_linker", "entity_ruler", "merge_noun_chunks",
                                                                "merge_entities", "merge_subtokens"])
                with open(self.corpus.xml_file_name, 'a+') as f:
                # f.write('<text name="%s">\n' % doc_name)
                    for sentence in doc.sents:
                        # lock.acquire()
                        f.write('<s>\n')
                        for token in sentence:
                            if isinstance(self.corpus, corpus.DiachronicCorpus):
                                f.write(
                                    token.text + '\t' + token.pos_ + '\t' + token.lemma_ + '\t' + token.tag_ + '\t' + token.dep_ + '\t' + diac + '\n')
                            else:
                                f.write(
                                    token.text + '\t' + token.pos_ + '\t' + token.lemma_ + '\t' + token.tag_ + '\t' + token.dep_ + '\n')
                        f.write('</s>\n')
                        # lock.release()
                    # f.write('</text>\n')
            except:
                print('Va bé cazzi')

    def read_document(self, archive, member):
        file = archive.extractfile(member)
        chunk = file.read(1000)
        while chunk:
            yield chunk
            chunk = file.read(1000)

    # def write_on_file(self, doc, doc_name, year=None):
    #     with open(self.corpus.xml_file_name, 'a+') as f:
    #         f.write('<text>\n')
    #         for sentence in doc.sents:
    #             f.write('<s>\n')
    #             for token in sentence:
    #                 if isinstance(self.corpus, corpus.DiachronicCorpus):
    #                     f.write(
    #                         token.text + '\t' + token.pos_ + '\t' + token.lemma_ + '\t' + token.tag_ + '\t' + token.dep_ + '\t' + year + '\n')
    #                 else:
    #                     f.write(
    #                         token.text + '\t' + token.pos_ + '\t' + token.lemma_ + '\t' + token.tag_ + '\t' + token.dep_ + '\n')
    #             f.write('</s>\n')
    #         f.write('</text>\n')

    @staticmethod
    def diachronic_attribute(file_name):
        diac = ''
        for c in file_name:
            if c.isdigit():
                diac += c
        return diac

    def read_files(self, document):

        def process_string(strline):
            line = strline.decode('utf-8')
            words = line.split('\t')
            first_word = words[0].split()
            del (words[0])
            alpha = [c for c in first_word if c == ' ' or c.isalpha() or c == '-' or c == '\'']
            if len(alpha) > 0:
                line = (' '.join(alpha) + '\n').encode('utf-8')
                line = simple_preprocess(line, deacc='True', min_len=2, max_len=20)
                if len(line) > 0:
                    line = ' '.join(line)
                else:
                    return ''
            else:
                return ''

            for word in words:
                line += '\t' + word

            return line

        text = ''
        input = gzip.open(self.corpus.documents_path + os.sep + document)
        for line in input:
            text += process_string(line)
        input.close()

        return text, document

    def format_sentence(self, dictionary):
        string = ''

        for token in dictionary:
            if isinstance(self.corpus, corpus.DiachronicCorpus):
                string += token[0] + '\t' + token[1] + '\t' + token[2] + '\t' + token[3] + '\t' + token[4] + token[
                    5] + '\n'
            else:
                string += token[0] + '\t' + token[1] + '\t' + token[2] + '\t' + token[3] + '\t' + token[4] + '\n'

        return string
