import multiprocessing
import os
from time import time
from gensim.models import Word2Vec
import pickle
import numpy as np
import math
from collections import Counter
import gc
import scipy.sparse as sparse
from threading import Thread
import random
from scripts.bernoulli.models import bern_emb_model
from utilities import Utilities


class BinThread(Thread):
    def __init__(self, f):
        Thread.__init__(self)
        self.f = f

    def run(self):
        self.f()


class Bin(object):
    def __init__(self, name):
        self.name = name


class Bernoulli(Bin):
    def __init__(self, name, tok2indx, corpus, savepath, dictionary, years_range, unig=None, sentences=None,
                 examplespath=None, conf={}):
        self.name = name
        self.sentences = sentences
        self.savepath = savepath
        self.tok2indx = tok2indx
        self.unigram = unig
        self.examplespath = examplespath
        newconf = dict(conf)
        newconf['dynamic'] = False
        newconf['logdir'] = savepath
        newconf['n_words'] = len(dictionary)
        newconf['n_bins'] = len(years_range)
        unigram = np.array(unig)
        unigram = (1.0 * unigram / sum(unigram)) ** (3.0 / 4)
        newconf['unigram'] = unigram
        if 'minibatch' in newconf.keys():
            self.mb = newconf['minibatch']
            newconf['minibatch'] = np.array([self.mb])
        if type(corpus).__name__ == 'Ngram':
            newconf['ngram'] = True
        else:
            newconf['ngram'] = False
        self.conf = newconf

    def compute_examples(self):
        def prob(f, t):
            return ((f - t) / f) - math.sqrt(t / f)

        D = {}
        with open(self.savepath, 'wb+') as f:
            usum = sum(self.unigram)
            for s in self.sentences:
                if s[0] in self.tok2indx and s[1] in self.tok2indx:
                    s2 = [self.tok2indx[s[0]]]
                    if np.random.random() > prob(self.unigram[self.tok2indx[s[1]]] / usum, 1e-5):
                        s2.append(self.tok2indx[s[0]])
                        if tuple(s2) in D.keys():
                            D[tuple(s2)] = D[tuple(s2)] + 1
                        else:
                            D[tuple(s2)] = 1
            pickle.dump(D, f)

    def dim(self):
        with open(self.examplespath, 'rb') as f:
            D = pickle.load(f)
            return sum(list(D.values()))

    def load_examples(self):
        with open(self.examplespath, 'rb') as f:
            D = pickle.load(f)
            keys = list(D.keys())
            values = list(D.values())
            while (len(values) > 0):
                indx = random.randint(0, len(values) - 1)
                values[indx] = values[indx] - 1
                k = list(keys[indx])
                if values[indx] == 0:
                    del values[indx]
                    del keys[indx]
                yield k

    def train(self):

        mb = self.mb

        def batch():
            examples = self.load_examples()
            b = []
            for s in examples:
                if len(b) <= mb - 1:
                    b.append(s)
                else:
                    yield np.array(b, dtype=np.int32)
                    b = []
                    b.append(s)

        self.conf['f_batch'] = batch
        self.conf['n_examples'] = self.dim()

        model = bern_emb_model(self.conf)
        model.initialize_training()
        model.train_embeddings()

    def load(self):
        with open(self.savepath, 'rb') as f:
            return pickle.load(f)['rho']


class PMI(Bin):
    def __init__(self, name, conf, sentences, tok2indx, indx2tok, savepath):
        self.name = name
        self.front_window = conf['front_window']
        self.back_window = conf['back_window']
        self.sentences = sentences
        self.savepath = savepath
        self.tok2indx = tok2indx
        self.indx2tok = indx2tok

    def train(self):
        skipgram_counts = Counter()
        iterator = self.sentences.__iter__()
        for line in iterator:
            isInD = [True if v in self.tok2indx.keys() else False for v in line]
            for ifw, fw in enumerate(line):
                if isInD[ifw]:
                    icw_min = max(0, ifw - self.back_window)
                    icw_max = min(len(line) - 1, ifw + self.front_window)
                    icws = [ii for ii in range(icw_min, icw_max + 1) if ii != ifw]
                    for icw in icws:
                        if isInD[icw]:
                            skipgram = (line[ifw], line[icw])
                            skipgram_counts[skipgram] += 1
        print('number of skipgrams: {}'.format(len(skipgram_counts)))
        print('most common: {}'.format(skipgram_counts.most_common(10)))
        row_indxs = []
        col_indxs = []
        dat_values = []
        ii = 0
        for (tok1, tok2), sg_count in skipgram_counts.items():
            ii += 1
            if ii % 1000000 == 0:
                print(f'finished {ii / len(skipgram_counts):.2%} of skipgrams')
            tok1_indx = self.tok2indx[tok1]
            tok2_indx = self.tok2indx[tok2]

            row_indxs.append(tok1_indx)
            col_indxs.append(tok2_indx)
            dat_values.append(sg_count)

        wwcnt_mat = sparse.csr_matrix((dat_values, (row_indxs, col_indxs)))

        num_skipgrams = wwcnt_mat.sum()
        assert (sum(skipgram_counts.values()) == num_skipgrams)

        # for creating sparce matrices
        row_indxs = []
        col_indxs = []

        ppmi_dat_values = []

        sum_over_words = np.array(wwcnt_mat.sum(axis=0)).flatten()
        sum_over_contexts = np.array(wwcnt_mat.sum(axis=1)).flatten()

        ii = 0
        for (tok1, tok2), sg_count in skipgram_counts.items():
            ii += 1
            if ii % 1000000 == 0:
                print(f'finished {ii / len(skipgram_counts):.2%} of skipgrams')
            tok1_indx = self.tok2indx[tok1]
            tok2_indx = self.tok2indx[tok2]

            nwc = sg_count
            Pwc = nwc / num_skipgrams
            nw = sum_over_contexts[tok1_indx]
            Pw = nw / num_skipgrams
            nc = sum_over_words[tok2_indx]
            Pc = nc / num_skipgrams

            pmi = np.log2(Pwc / (Pw * Pc))
            ppmi = max(pmi, 0)

            row_indxs.append(tok1_indx)
            col_indxs.append(tok2_indx)
            ppmi_dat_values.append(ppmi)

        gc.collect()
        ppmi_mat = sparse.csr_matrix((ppmi_dat_values, (row_indxs, col_indxs)),
                                     shape=(len(self.tok2indx), len(self.tok2indx)))
        with open(self.savepath, 'wb+') as handle:
            pickle.dump(ppmi_mat, handle, protocol=pickle.HIGHEST_PROTOCOL)

        print('done')

    def load(self):
        with open(self.savepath, 'rb') as f:
            return pickle.load(f)


class W2V(Bin):
    def __init__(self, name, conf, sentences, savepath):
        self.name = name
        self.min_count = conf['min_count']
        self.window = conf['window']
        self.size = conf['size']
        self.sample = conf['sample']
        self.negative = conf['negative']
        self.epochs = conf['epochs']
        self.alpha = conf['alpha']
        self.sentences = sentences
        self.savepath = savepath

    def train(self):
        cores = multiprocessing.cpu_count()  # Coudnt the number of cores in a computer

        w2v_model = Word2Vec(min_count=self.min_count,
                             window=self.window,
                             size=self.size,
                             sample=self.sample,
                             negative=self.negative,
                             alpha=self.alpha,
                             workers=cores,
                             sg=1)
        t = time()

        w2v_model.build_vocab(self.sentences, progress_per=10000)
        print('Year ' + str(self.name))
        print('Time to build vocab: {} mins'.format(round((time() - t) / 60, 2)))
        t = time()

        w2v_model.train(self.sentences, total_examples=w2v_model.corpus_count, epochs=self.epochs, report_delay=1)

        print('Time to train the model: {} mins'.format(round((time() - t) / 60, 2)))
        w2v_model.save(self.savepath)

    def load(self, D):
        model = Word2Vec.load(self.savepath).wv

        def get_Matrix(d, m):
            M = []
            for w in d:
                try:
                    M.append(m[w])
                except:
                    print('word %s not in vocabulary' % w)
            return np.array(M)

        return get_Matrix(D, model)


class BinCollection:

    def __init__(self, corpus, conf, years_range, split, dictionary, mode='WORD2VEC', unigram=None):
        self.corpus = corpus
        u = Utilities(self.corpus)
        years = u.load_years(years_range, self.corpus.corpus_path, split)
        nbins = len(years)
        self.binpath = self.corpus.corpus_path + os.path.sep + 'bin' + os.path.sep
        i = 0
        # yname = years_range[0]
        self.collection = []
        if mode == 'WORD2VEC':
            if not os.path.exists(self.binpath + 'w2v' + os.sep):
                os.makedirs(self.binpath + 'w2v' + os.sep)
                for year in years:
                    name = str(years_range[i]) #str(yname) + '-' + str(yname + split - 1)
                    savepath = self.binpath + 'w2v' + os.sep + name + '.gensim'
                    sentences = self.corpus.get_sentences(year[0])
                    w2v = W2V(name, conf, sentences, savepath)
                    w2v.train()
                    m = w2v.load(list(dictionary.values()))
                    self.collection.append(m)
                    i += 1
                    # yname = yname + split
            else:
                for year in years:
                    name = str(years_range[i]) # str(yname) + '-' + str(yname + split - 1)
                    savepath = self.binpath + 'w2v' + os.path.sep + name + '.gensim'
                    w2v = W2V(name, conf, None, savepath)
                    m = w2v.load(list(dictionary.values()))
                    self.collection.append(m)
                    i += 1
                    # yname = yname + split
        elif mode == 'PMI':
            if not os.path.exists(self.binpath + 'pmi' + os.path.sep):
                os.makedirs(self.binpath + 'pmi' + os.path.sep)
                indx2tok = self.corpus.D
                tok2indx = {indx2tok[k]: k for k in indx2tok.keys()}
                for year in years:
                    name = str(yname) + '-' + str(yname + self.corpus.split - 1)
                    savepath = self.binpath + 'pmi' + os.path.sep + name + '.pickle'
                    sentences = self.corpus.get_sentences(year)
                    pmi = PMI(name, conf, sentences, tok2indx, indx2tok, savepath)
                    t = BinThread(pmi.train)
                    t.start()
                    t.join()
                    yname = yname + self.corpus.split
            else:
                for year in years:
                    name = str(yname) + '-' + str(yname + self.corpus.split - 1)
                    savepath = self.binpath + 'pmi' + os.path.sep + name + '.pickle'
                    pmi = PMI(name, conf, None, None, None, savepath)
                    self.collection.append(pmi.load())
                    yname = yname + self.corpus.split
        elif mode == 'BERNOULLI':
            if not os.path.exists(self.binpath + 'bernoulli' + os.path.sep + 'examples'):
                os.makedirs(self.binpath + 'bernoulli' + os.path.sep + 'examples')
                indx2tok = dictionary
                tok2indx = {indx2tok[k]: k for k in indx2tok.keys()}
                for year in years:
                    name = str(yname) + '-' + str(yname + split - 1)
                    savepath = self.binpath + 'bernoulli' + os.sep + 'examples' + os.sep + name + '.pickle'
                    sentences = self.corpus.get_sentences(year)
                    ber = Bernoulli(name, tok2indx, self.corpus, savepath, unigram, dictionary, years_range, sentences)
                    t = BinThread(ber.compute_examples)
                    t.start()
                    t.join()
                    yname = yname + split
            if not os.path.exists(self.binpath + 'bernoulli' + os.sep + 'models'):
                os.makedirs(self.binpath + 'bernoulli' + os.sep + 'models')
                indx2tok = dictionary
                tok2indx = {indx2tok[k]: k for k in indx2tok.keys()}
                for i in range(nbins):
                    name = str(yname) + '-' + str(yname + split - 1)
                    savepath = self.binpath + 'bernoulli' + os.sep + 'models' + os.sep + 'model_' + str(
                        i) + os.sep
                    os.makedirs(savepath)
                    bp = self.binpath + 'bernoulli' + os.path.sep + 'examples' + os.path.sep + name + '.pickle'
                    ber = Bernoulli(name, tok2indx, self.corpus, savepath, unigram, dictionary, years_range,
                                    examplespath=bp, conf=conf)
                    ber.train()
                    yname = yname + split
            else:
                for i in range(nbins):
                    name = str(yname) + '-' + str(yname + self.corpus.split - 1)
                    savepath = self.binpath + 'bernoulli' + os.sep + 'models' + os.sep + 'model_' + str(
                        i) + os.sep + 'variational0.dat'
                    bp = self.binpath + 'bernoulli' + os.sep + 'examples' + os.sep + name + '.pickle'
                    ber = Bernoulli(name, [], self.corpus, savepath, unigram, dictionary, years_range, examplespath=bp)
                    self.collection.append(ber)
                    yname = yname + split
        else:
            print('BinCollection mode not recognized')

    def get(self):
        return self.collection
